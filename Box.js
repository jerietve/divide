/**
 * Boxes are the objects that in the game of Divide need to be distributed among containers. They are currently always
 * square, but in the code they have a width and height. This class defines methods related to a single box, such as
 * drawing it, checking if a point is within the box, changing its color dependent on its select state etc. The default
 * colors for different depths (values such as 1, 0.1, 0.001) of boxes are also defined in here. Boxes have two sizes:
 * their default size, which is the size they will always have when in free space, and their current size, which may
 * be different if they have to fit inside a container.
 * 
 * @returns {Box}	A new box of default size 50x50 and depth 0 (value 1)
 */
function Box()
{
	this.freeWidth = 50;				// The size when outside a container
	this.freeHeight = 50;				// The size when outside a container
	this.width = 50;					// The current draw size
	this.height = 50;					// The current draw size
	this.lineWidth = 2;
	this.color = { r: 250, g: 128, b: 128 };
	this.depth = 0;	//depth 0 means value 1, depth 1 means value 0.1, etc
	this.depthColors = {0: {r: 250, g: 128, b: 128},
			1: {r: 128, g: 250, b: 179},
			2: {r: 41, g: 240, b: 236},
			3: {r: 252, g: 195, b: 61}};


	/**
	 * Checks whether the coordinates given are within the outer bounds of the box
	 *
	 * @param {Point}	point	The point to check
	 *
	 * @returns {Boolean}	True in case the coordinates are within the inner bounds, false otherwise
	 */
	this.withinBounds = function(point)
	{
		var xOkay = point.x > this.absolutePosition.x && point.x < this.absolutePosition.x + this.width + this.lineWidth * 2;
		var yOkay = point.y > this.absolutePosition.y && point.y < this.absolutePosition.y + this.height + this.lineWidth * 2;
		return xOkay && yOkay;
	};
	
	
	/**
	 * Changes the color of this box to reflect the fact that is has been selected.
	 * 
	 * @returns nothing
	 */
	this.highlight = function()
	{
		// create a new modified color object; modifying the old one would change the original depth colors
		this.color = {
			r: Math.min(Math.floor(this.depthColors[this.depth].r * 1.5), 255),
			g: Math.min(Math.floor(this.depthColors[this.depth].g * 1.5), 255),
			b: Math.min(Math.floor(this.depthColors[this.depth].b * 1.5), 255)
		};
	};


	/**
	 * Changes the color of this box back to its original color.
	 * 
	 * @returns nothing
	 */
	this.resetColor = function()
	{
		this.color = this.depthColors[this.depth];
	};


	/**
	 * Draws this box 
	 * 
	 * @returns nothing
	 */
	this.draw = function()
	{
		var c = context;
		if(c === undefined)
		{
			throw Error("Box.draw(): No context defined!");
		}

		var gradient = c.createLinearGradient(this.absolutePosition.x, this.absolutePosition.y,
				this.absolutePosition.x + this.width, this.absolutePosition.y + this.height);
		var colorLight = "rgba(" + (this.color.r - 50) + ", " + (this.color.g - 50) + ", " + (this.color.b - 50) + ", 1)";
		var colorDark = "rgba(" + this.color.r + ", " + this.color.g + ", " + this.color.b + ", 1)";
		gradient.addColorStop(0, colorDark);
		gradient.addColorStop(0.5, colorLight);
		gradient.addColorStop(1, colorDark);
		c.fillStyle = gradient;
		c.fillRect(
				this.absolutePosition.x + this.lineWidth,
				this.absolutePosition.y + this.lineWidth,
				this.width - 2 * this.lineWidth,
				this.height - 2 * this.lineWidth );
		c.strokeStyle = "rgba(" + (this.color.r + 50) + ", " + (this.color.g + 50) + ", " + (this.color.b + 50) + ", .5)";
		c.strokeRect(
				this.absolutePosition.x + this.lineWidth/2,
				this.absolutePosition.y + this.lineWidth/2,
				this.width - this.lineWidth,
				this.height - this.lineWidth );
	};


	/**
	 * Sets the position of the upper left corner of this box.
	 *
	 * @param {Point} point	The absolute location on the canvas
	 * @returns nothing
	 */
	this.setPosition = function(point)
	{
		this.absolutePosition = point;
		this.relativePosition = new Point((point.x + this.width / 2) / canvas.width,
				(point.y + this.height / 2) / canvas.height);
	};


	/**
	 * Returns the absolute position of the upper left corner of this box on the canvas
	 *
	 * @returns {Point} Absolute position
	 */
	this.getPosition = function()
	{
		return this.absolutePosition;
	};


	/**
	 * Recalculates the absolute position using the relataive position and the current canvas dimensions. Also resizes
	 * if necessary, around its center.
	 *
	 * @returns nothing
	 */
	this.recalculatePosition = function()
	{
		/* Resize the box first */
		this.width = this.height = getBoxSize(this.depth);
		/* The move its upper left corner to the new absolute position */
		this.absolutePosition = new Point(this.relativePosition.x * canvas.width - this.width / 2,
				this.relativePosition.y * canvas.height - this.height / 2);
	};


	/**
	 * Sets the depth and the right color.
	 *
	 * @param {Number} depth	The new depth
	 * @returns nothing
	 */
	this.setDepth = function(depth)
	{
		this.depth = depth;

		// if we try to set an unknown depth, set to random color
		if(this.depthColors[depth] === undefined)
		{
			Math.seed = depth;
			this.color = {r: Math.seededRandInt(), g: Math.seededRandInt(), b: Math.seededRandInt()};
		}
		else
		{
			this.color = this.depthColors[depth];
		}

	};
	
	
	/**
	 * Get an array of the corner points of the box, clockwise from the upper left corner.
	 * 
	 * @returns {Point[]}
	 */
	this.getCorners = function()
	{
		return [this.absolutePosition,
				this.absolutePosition.add(new Point(this.width, 0)),
				this.absolutePosition.add(new Point(this.width, this.height)),
				this.absolutePosition.add(new Point(0, this.height))];
	};


	/* PRIVATE */
	// The relative position of the center of this box in percentage of the canvas size
	this.relativePosition = new Point(0, 0);
	// The absolute position of the upper left corner of this box in pixels within the canvas
	this.absolutePosition = new Point(0, 0);
}
