/**
 * A BoxGroup instance handles a group of Box objectes, a group of boxes. Boxes can be added to and removed from the
 * group, and the group can be moved and drawn as a whole. When selecting a box group, what actually happens is that the
 * clicked box is selected, and the rest of the boxes are moved relative to this box.
 * 
 * @returns {BoxGroup}	A new empty box group
 */
function BoxGroup()
{
	/* The boxes in this box group */
	this.boxes = new Array();
	/* The relative position of the absolute position of all boxes with respect to the absolute position of the selected
	 * box, in pixels. */
	this.relativePositions = new Array();
	/* The currently selected box (may be undefined) */
	this.selectedBox;
	/* The position of the point where the selected box is grabbed relative to that box' position, in pixels */
	this.selectedBoxGrabPoint;


	/**
	 * Checks if the point is within any of the boxes in this group and selects it. Also updates all relative positions.
	 * Boxes are checked from top to bottom until the first hit.
	 *
	 * @param {Point} point
	 * @returns {Boolean} True if a box was selected, false otherwise
	 */
	this.selectBox = function(point)
	{
		this.selectedBox = undefined;

		/* Loop through the boxes from top to bottom, selecting the first one found */
		for(var i = this.boxes.length - 1; i >= 0; i--)
		{
			if(this.boxes[i].withinBounds(point))
			{
				this.selectedBox = this.boxes[i];
				this.selectedBoxGrabPoint = point.sub(this.selectedBox.getPosition());
				break;
			}
		}

		/* If a box was found, update the relative positions of all other boxes relative to this one and return true */
		if(this.selectedBox)
		{
			this.relativePositions.length = 0;
			for(var i = 0; i < this.boxes.length; i++)
			{
				this.relativePositions.push(this.boxes[i].getPosition().sub(this.selectedBox.getPosition()));
			}

			return true;
		}

		return false;
	};


	/**
	 * Add a box to this box group.
	 * 
	 * @param {Box} box	The box to add
	 * @returns nothing
	 */
	this.push = function(box)
	{
		this.boxes.push(box);
	};
	
	
	/**
	 * Removes first occurrence of box from BoxGroup. There should normally be at most one equal box in the box group.
	 * Do mind that removing a box from a box group does exactly that and nothing more. The box is not automatically
	 * added to the list of free boxes.
	 *
	 * @param {Box} box The box to remove
	 * @returns {Boolean} true iff the box was removed
	 */
	this.remove = function(box)
	{	
		for(var i = 0; i < this.boxes.length; i++)
		{
			if(this.boxes[i] === box)
			{
			   this.boxes.splice(i, 1);
			   return true;
			}
		}
		return false;
	};
	
	
	/**
	 * Removes the box at index from the box list
	 * 
	 * @param {int} index	Index of box to remove
	 * @returns nothing
	 */
	this.removeAt = function(index)
	{
		this.boxes.splice(index, 1);
	};


	/**
	 * Reset the box group, removing all boxes. The boxes will simply disappear.
	 * 
	 * @returns nothing
	 */
	this.clear = function()
	{
		this.boxes.length = 0;
	};


	/**
	 * The selected box' grab point is moved to point. All other boxes are moved relative to it.
	 *
	 * @param {Point} point	The point to move the grab point of the selected box to
	 * @returns nothing
	 */
	this.move = function(point)
	{
		for(var i = 0; i < this.boxes.length; i++)
		{
			var newPosition = point.sub(this.selectedBoxGrabPoint).add(this.relativePositions[i]);
			/* If this is outside of the canvas, fix it */
			if(newPosition.x < 0) newPosition.x = 0;
			else newPosition.x = Math.min(canvas.width - this.boxes[i].width, newPosition.x);
			if(newPosition.y < 0) newPosition.y = 0;
			else newPosition.y = Math.min(canvas.height - this.boxes[i].height, newPosition.y);

			this.boxes[i].setPosition(newPosition);
		}
	};


	/**
	 * Draw all boxes in this box group from bottom to top.
	 * 
	 * @returns nothing
	 */
	this.draw = function()
	{
		for(var i = 0; i < this.boxes.length; i++)
		{
			this.boxes[i].draw();
		}
	};
}
