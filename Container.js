/**
 * Containers are the other most important visible object in the Divide game, next to boxes. Containers are used to put
 * boxes into, which is the objective of the game. Containers have a position and an array of boxes which they contain.
 * Functions are provided for adding and removing boxes, checking whether a point is within the container and getting
 * the value of the boxes in the container combined.
 * 
 * Boxes inside containers are all the same size. They are arranged in rows from left to right, top to bottom. Hence,
 * there are always spots available for 9, 16, or any other square boxes. The boxes are always ordered by value from
 * high to low (or by depth from low to high). There is always at least one empty slot available in the lower right
 * corner. There are never more than ten boxes of the same depth in a container, except for the lowest depth (boxes of
 * value 1). Any group of ten low value boxes is combined immediately into one box of a higher order of magnitude. 
 * 
 * @returns {Container}
 */
function Container()
{
	/* The upper left point of this container in the canvas */
	this.position = new Point(0, 0);
	/* The size of an edge of the container (containers are square) */
	this.edgeLength = 50;
	/* Properties used for drawing containers */
	this.strokeStyle = "rgba(200, 200, 255, 1)";
	this.darkGradient1 = "rgba(50, 50, 100, 1)";
	this.darkGradient2 = "rgba(150, 150, 200, 1)";
	this.darkGradient3 = "rgba(50, 50, 100, .5)";
	this.darkGradient4 = "rgba(150, 150, 200, .5)";
	this.lineWidth = 4;
	this.lineCap = "round";
	this.lineJoin = "round";
	/* The boxes currently inside this container */
	this.boxes = new Array();


	/**
	 * Adds a new box to the list of boxes in this container. Do not add the same box more than once. Boxes are
	 * inserted in a place such that they remain ordered by depth.
	 *
	 * @param {Box} box	The new box
	 *
	 * @returns nothing
	 */
	this.addBox = function(box)
	{
		/* Boxes have a maximum area of one ninth the are of the inside of the container. All boxes always have the same
		 * size. This area is always one 9th, 16th, etc (but rather not) of the container inner area. */
		var num_boxes = this.boxes.length + 1;
		var boxesPerLine = Math.ceil(Math.sqrt(Math.max(num_boxes + 1, 9)));
		/* If adding this box would fill the container, the size of all boxes must be changed */
		if(Math.ceil(Math.sqrt(this.boxes.length + 1)) !== boxesPerLine)
		{
			// TODO optimization: currently they're all resized and moved here and possibly all moved again later in case a box is spliced in
			for(var i = 0; i < this.boxes.length; i++)
			{
				var b = this.boxes[i];
				b.width = b.height = (this.edgeLength - 2 * this.lineWidth) / boxesPerLine;
				var x = this.position.x + this.lineWidth + (i % boxesPerLine) * b.width;
				var y = this.position.y + this.lineWidth + (Math.floor(i / boxesPerLine)) * b.height;
				b.setPosition(new Point(x, y));
			}
		}
		box.width = box.height = (this.edgeLength - 2 * this.lineWidth) / boxesPerLine;

		/* Set the location of the new box */
		// If it's the first box, just add it
		if(this.boxes.length === 0)
		{
			var x = this.position.x + this.lineWidth;
			var y = this.position.y + this.lineWidth;
			box.setPosition(new Point(x, y));
			this.boxes.push(box);
		}
		else
		{
			// Find the maximum depth of the boxes currently in the container
			var maxDepth = 0;
			for(var i = 0; i < this.boxes.length; i++)
			{
				maxDepth = Math.max(maxDepth, this.boxes[i].depth);
			}
			// If this box has a depth equal to or greater than the max depth, just add it at the end
			if(box.depth >= maxDepth)
			{
				var x = this.position.x + this.lineWidth + ((num_boxes - 1) % boxesPerLine) * box.width;
				var y = this.position.y + this.lineWidth + (Math.floor((num_boxes - 1) / boxesPerLine)) * box.height;
				box.setPosition(new Point(x, y));
				this.boxes.push(box);
			}
			else
			{
				/* We're gonna have to move some other boxes around as well because the box needs to be inserted in the
				 * middle somewhere. Because boxes are put in the array in the same order as they have in the container,
				 * the ones to be moved are at the end of the array behind the new box. */
				for(var i = this.boxes.length - 1; i >= 0; i--)
				{
					var deepBox = this.boxes[i];
					// Only move boxes with a higher depth and stop once we reach one with equal or lower depth
					if(deepBox.depth <= box.depth) break;
					// Move the box
					var x = this.position.x + this.lineWidth +
							((num_boxes - (this.boxes.length - i)) % boxesPerLine) * deepBox.width;
					var y = this.position.y + this.lineWidth +
							(Math.floor((num_boxes - (this.boxes.length - i)) / boxesPerLine)) * deepBox.height;
					deepBox.setPosition(new Point(x, y));
				}

				/* Insert the new box, using the fact that i is still alive here. If i is 3, the new box is inserted in
				 * position 4. */
				this.boxes.splice(i + 1, 0, box);
				var x = this.position.x + this.lineWidth + ((num_boxes - (this.boxes.length - i - 1)) % boxesPerLine) * box.width;
				var y = this.position.y + this.lineWidth + (Math.floor((num_boxes - (this.boxes.length - i - 1)) / boxesPerLine)) * box.height;
				box.setPosition(new Point(x, y));
			}
		}
		this.recombineBoxes();
	};

	
	/**
	 * Remove the box in positon index in the boxes array from that array. This will remove the box from the container.
	 * The box just disappears. If you want it to reappear somewhere else, add it yourself.
	 * 
	 * @param {Number} index	The index in the boxes array of the box to remove
	 * @returns nothing
	 */
	this.removeBox = function(index)
	{
		//num_boxes represents the new amount of boxes after the removal
		var boxesPerLineOld = Math.ceil(Math.sqrt(Math.max(this.boxes.length + 1, 9)));
		var num_boxes = this.boxes.length - 1;
		var boxesPerLineNew = Math.ceil(Math.sqrt(Math.max(num_boxes + 1, 9)));

		// remove box
		this.boxes.splice(index, 1);

		if(boxesPerLineNew !== boxesPerLineOld)
		{
			/* Resize and move all boxes */
			for(var i = 0; i < this.boxes.length; i++)
			{
				var b = this.boxes[i];
				b.width = b.height = (this.edgeLength - 2 * this.lineWidth) / boxesPerLineNew;
				var x = this.position.x + this.lineWidth + (i % boxesPerLineNew) * b.width;
				var y = this.position.y + this.lineWidth + (Math.floor(i / boxesPerLineNew)) * b.height;
				b.setPosition(new Point(x, y));
			}
		}
		else
		{
			/* Move only the boxes behind the removed one */
			for(var j = index; j < this.boxes.length; j++)
			{
				var x = this.position.x + this.lineWidth + (j % boxesPerLineNew) * this.boxes[j].width;
				var y = this.position.y + this.lineWidth + (Math.floor(j / boxesPerLineNew)) * this.boxes[j].height;
				this.boxes[j].setPosition(new Point(x, y));
			}
		}
	};


	/**
	 * Checks whether the coordinates given are within the inner bounds of the container
	 *
	 * @param {Point}	point	The point to check
	 *
	 * @returns {Boolean}	True in case the coordinates are within the inner bounds, false otherwise
	 */
	this.withinBounds = function(point)
	{
		var xOkay = point.x > this.position.x + this.lineWidth && point.x < this.position.x + this.edgeLength + this.lineWidth;
		var yOkay = point.y > this.position.y + this.lineWidth && point.y < this.position.y + this.edgeLength + this.lineWidth;
		return xOkay && yOkay;
	};
	
	
	/**
	 * Checks if more than 50% of the area of the box is over this container, or 30% in a corner case. If so, the 
	 * container 'captures' the box.
	 * 
	 * @param {type} box	The box to check for
	 * 
	 * @returns {Boolean} true iff the container captures the box
	 */
	this.captures = function(box)
	{
		/* Boxes are always smaller than containers. First find the corners of the box which are inside the container */
		var corners = box.getCorners();
		var containedCorners = new Array();
		for(var i = 0; i < corners.length; i++)
		{
			if(this.withinBounds(corners[i])) containedCorners.push(i);
		}
		/* There are either zero, one, two or four corners of the box inside the container. Zero simply means the box
		 * is not captured, four means it is. The other cases are harder. */
		if(containedCorners.length === 0) return false;
		if(containedCorners.length === 4) return true;
		if(containedCorners.length === 2)
		{
			/* A rectangle of the box is inside, another rectangle is outside. Simply finding the relative position on
			 * the length of the edge of the box from the corner to the point where it crosses the container border is
			 * enough. */
			var insideCorner = corners[containedCorners[1]];
			var outsideCorner = containedCorners[0] === 0 && containedCorners[1] === 3 ? corners[1] : corners[(containedCorners[1] + 1) % 4];
			if(containedCorners[0] === 0 && containedCorners[1] === 1)
			{
				/* Top side of box inside container. Check with bottom of container */
				return this.position.y + this.edgeLength - insideCorner.y > outsideCorner.y - (this.position.y + this.edgeLength);
			}
			else if(containedCorners[0] === 1)
			{
				/* Right side of box inside container. Check with left of container */
				return insideCorner.x - this.position.x > this.position.x - outsideCorner.x;
			}
			else if(containedCorners[0] === 2)
			{
				/* Bottom of box inside container. Check with top of container */
				return insideCorner.y - this.position.y > this.position.y - outsideCorner.y;
			}
			else if(containedCorners[0] === 0  && containedCorners[1] === 3)
			{
				/* Left side of box inside container. Check with right of container */
				return this.position.x + this.edgeLength - insideCorner.x > outsideCorner.x - (this.position.x + this.edgeLength);
			}
		}
		
		/* Only one corner point is inside. Calculate the size of the rectangle inside the container, and compare to the
		 * total size of the box. */
		var insideCorner = corners[containedCorners[0]];
		var CWOutsideCorner = corners[(containedCorners[0] + 1) % 4];
		var CCWOutsideCorner = corners[(containedCorners[0] + 3) % 4];
		var sameXCorner = CWOutsideCorner.x === insideCorner.x ? CWOutsideCorner : CCWOutsideCorner;
		var sameYCorner = CWOutsideCorner.y === insideCorner.y ? CWOutsideCorner : CCWOutsideCorner;
		/* Get the length of the horizontal edge */
		var horizontalEdgeLength, verticalEdgeLength;
		if(insideCorner.x < sameYCorner.x) horizontalEdgeLength = this.position.x + this.edgeLength - insideCorner.x;
		else horizontalEdgeLength = insideCorner.x - this.position.x;
		/* Get the length of the vertical edge */
		if(insideCorner.y < sameXCorner.y) verticalEdgeLength = this.position.y + this.edgeLength - insideCorner.y;
		else verticalEdgeLength = insideCorner.y - this.position.y;
		/* Calculate the size of the rectangle inside the container */
		var insideRectangle = horizontalEdgeLength * verticalEdgeLength;
		/* Compare to the size of the box */
		return box.width * box.height < insideRectangle / 0.3;
	};


	/**
	 * Calculates the combined value of all contained boxes.
	 *
	 * @returns {Number} The value of all contained boxes summed.
	 */
	this.getContainedValue = function()
	{
		var value = 0;

		for(var i = 0; i < this.boxes.length; i++)
		{
			value += 1 / Math.pow(10, this.boxes[i].depth);
		}

		return value;
	};


	/**
	 * Returns the highest depth of any box contained in this container or 0 in case of no boxes.
	 *
	 * @returns {Number}
	 */
	this.getMaxDepth = function()
	{
		var maxDepth = 0;
		for(var i = 0; i < this.boxes.length; i++)
		{
			if(this.boxes[i].depth > maxDepth) maxDepth = this.boxes[i].depth;
		}
		return maxDepth;
	};


	/**
	 * Draws the container and all boxes contained within it.
	 *
	 * @returns nothing
	 */
	this.draw = function()
	{
		var c = context;
		if(c === undefined)
		{
			throw Error("Box.draw(): No context defined!");
		}

		// horizontal fill
		var gradient = c.createLinearGradient(
				this.position.x + this.lineWidth,
				this.position.y + this.lineWidth,
				this.position.x + this.edgeLength - 2 * this.lineWidth,
				this.position.y + this.lineWidth );
		gradient.addColorStop(0, this.darkGradient1);
		gradient.addColorStop(0.2, this.darkGradient2);
		c.fillStyle = gradient;
		c.fillRect(
				this.position.x + this.lineWidth,
				this.position.y + this.lineWidth,
				this.edgeLength - 2 * this.lineWidth,
				this.edgeLength - 2 * this.lineWidth );

		// vertical fill
		var gradient = c.createLinearGradient(
				this.position.x + this.lineWidth,
				this.position.y + this.lineWidth,
				this.position.x + this.lineWidth,
				this.position.y + this.edgeLength - 2 * this.lineWidth );
		gradient.addColorStop(0, this.darkGradient3);
		gradient.addColorStop(0.2, this.darkGradient4);
		c.fillStyle = gradient;
		c.fillRect(
				this.position.x + this.lineWidth,
				this.position.y + this.lineWidth,
				this.edgeLength - 2 * this.lineWidth,
				this.edgeLength - 2 * this.lineWidth );

		// stroke
		c.strokeStyle = this.strokeStyle;
		c.lineWidth = this.lineWidth;
		c.lineCap = this.lineCap;
		c.lineJoin = this.lineJoin;
		c.strokeRect(
				this.position.x + this.lineWidth/2,
				this.position.y + this.lineWidth/2,
				this.edgeLength - this.lineWidth,
				this.edgeLength - this.lineWidth );

		for(var i = 0; i < this.boxes.length; i++)
		{
			this.boxes[i].draw();
		}
	};


	/**************************************************** PRIVATE *****************************************************/
	

	/**
	 * Makes sure that no ten boxes of the same depth deeper than zero can be in the same container. As many groups of
	 * ten boxes of the same depth > 0 as possible are combined into one box of a lower depth, going from deep to
	 * shallow. After calling this function, no ten boxes of the same depth > 0 will be in the container, and the value
	 * of the boxes summed up will still be the same. Boxes will remain ordered by depth.
	 * 
	 * @returns nothing
	 */
	this.recombineBoxes = function()
	{
		var totalBoxesPerDepth = new Array();

		for(var i = 0; i < this.boxes.length; i++)
		{
			var depth = this.boxes[i].depth;
			if(totalBoxesPerDepth[depth] === undefined) totalBoxesPerDepth[depth] = 1;
			else totalBoxesPerDepth[depth]++;
		}

		/* Go from deep to shallow because ten deep ones will add a more shallow one. Skip depth 0. */
		for(var depth = totalBoxesPerDepth.length - 1; depth > 0; depth--)
		{
			/* There might be twenty or more, keep on removing them in groups of ten */
			while(totalBoxesPerDepth[depth] >= 10)
			{
				/* Remove ten of these boxes */
				var removed = 0;
				var index = this.boxes.length - 1;
				while(removed < 10)
				{
					if(this.boxes[index].depth === depth)
					{
						this.removeBox(index);
						totalBoxesPerDepth[depth]--;
						removed++;
					}
					index--;
				}
				/* Add one of less depth */
				var box = new Box();
				box.setDepth(depth - 1);
				this.addBox(box);
			}
		}
	};


	/**
	 * Repositions and resizes all boxes.
	 *
	 * @returns {undefined}
	 */
	this.sizeBoxes = function()
	{
		var boxesPerLine = Math.ceil(Math.sqrt(Math.max(this.boxes.length + 1, 9)));
		var boxWidth = (this.edgeLength - 2 * this.lineWidth) / boxesPerLine;
		for(var i = 0; i < this.boxes.length; i++)
		{
			var b = this.boxes[i];
			b.width = b.height = boxWidth;
			var x = this.position.x + this.lineWidth + (i % boxesPerLine) * b.width;
			var y = this.position.y + this.lineWidth + (Math.floor(i / boxesPerLine)) * b.height;
			b.setPosition(new Point(x, y));
		}
	};
}
