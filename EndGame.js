/**
 * Endgame is a box controller which is activated in level 5 when all but the last orange box have been divided among
 * the containers. A button will appear that will end the game. The button is only visible when there is one orange free
 * box. When the button is clicked, the single free box is continuously divided into 10 smaller ones, distributed over
 * the containers, etc. After one minute, the game fades to black.
*/

function EndGame()
{
	this.animating = false;
	this.startTime = 0;
	this.duration = 60000;
	this.state = "NONE";
	this.button = undefined;

	/**
	 * Determines whether the ending button needs to be shown, and does it, otherwise hides it.
	 */

	this.maybeShowButton = function()
	{
		for(var i = 0; i < game.containers.length; i++)
		{
			if(game.containers[i].getContainedValue() !== 1/10*3 + 1/100*3 + 1/1000*3)
			{
				this.hideEndButton();
				return;
			}
		}
		
		this.showEndButton();
	};
	
	/**
	 * Shows the ending button, with click handler which starts the ending animation.
	 */
	
	this.showEndButton = function()
	{
		if(!this.button)
		{
			this.button = document.createElement('button');
			this.button.innerHTML = "Solve";
			this.button.style.position = 'absolute';
			document.body.appendChild(this.button);
			
			this.button.onclick = function() {
				endGame.startAnimation();
				endGame.hideEndButton();
			};
		}

		this.button.style.top = Math.ceil(canvas.height / 3) + 'px';
		this.button.style.left = Math.ceil(canvas.width / 2) + 'px';
		this.button.style.display = 'block';
	};

	/**
	 * Hides the ending button
	 */

	this.hideEndButton = function()
	{
		if(this.button)
		{
			this.button.style.display = 'none';
		}
	};

	/**
	 * Starts the ending animation
	 */
	
	this.startAnimation = function()
	{
		// disable input
		game.destroyMultiSelectGroup();
		input.enabled = false;

		// enable animation
		this.animating = true;
		this.startTime = (new Date()).getTime();
		this.state = "MOVE_ONE_BOX";
	};

	/**
	 * If animation started, draw ending animation
	 *
	 * Animation states (each one second in duration):
	 *
	 * MOVE_ONE_BOX
	 * SPLIT_BOX
	 * MOVE_BOXES_1
	 * MOVE_BOXES_2
	 * MOVE_BOXES_3
	 */

	this.drawFrame = function(time)
	{
		var t = time % 1000; // one-second timer
		
		if(time % 5000 < 1000) // MOVE_ONE_BOX
		{
			// add boxes 0, 1 and 2 to container 2
			if(this.addOnce2 && !this.addOnce3)
			{
				this.addOnce2 = false;
				var boxes = game.freeBoxes.splice(0, 3);
				for(var i = 0; i < boxes.length; i++)
				{
					game.containers[2].addBox(boxes[i]);
				}
				
				this.addOnce3 = true;
			}
			
			// interpolate box position
			var newPos = new Point(Math.ceil(canvas.width / 2), Math.ceil(canvas.height / 3));
			var oldPos = game.freeBoxes[0].getPosition();
			var x = Math.ceil(oldPos.x + (newPos.x - oldPos.x) * (t / 1000));
			var y = Math.ceil(oldPos.y + (newPos.y - oldPos.y) * (t / 1000));

			// move box
			game.freeBoxes[0].setPosition(new Point(x, y));
		}
		else if(time % 5000 < 2000) // SPLIT_BOX
		{
			this.addOnce3 = false;
			
			if(game.freeBoxes.length === 1)
			{
				game.splitBox(0, true, true);
				this.splitOnce = true;
			}
		}
		else if(time % 5000 < 3000) // MOVE_BOXES_1
		{
			if(game.freeBoxes.length === 1)
			{
				game.splitBox(0, true, true);
				this.splitOnce = true;
			}
			
			// move boxes 0, 1 and 2 to container 0
			for(var i = 0; i < 3; i++)
			{
				// interpolate box position
				var newPos = game.containers[0].position;
				var oldPos = game.freeBoxes[i].getPosition();
				var x = Math.ceil(oldPos.x + (newPos.x - oldPos.x) * (t / 1000));
				var y = Math.ceil(oldPos.y + (newPos.y - oldPos.y) * (t / 1000));

				// move box
				game.freeBoxes[i].setPosition(new Point(x, y));
			}
		}
		else if(time % 5000 < 4000) // MOVE_BOXES_2
		{
			if(game.freeBoxes.length === 1)
			{
				game.splitBox(0, true, true);
				this.splitOnce = true;
			}
			
			// add boxes 0, 1 and 2 to container 0
			if(!this.addOnce1)
			{
				var boxes = game.freeBoxes.splice(0, 3);
				for(var i = 0; i < boxes.length; i++)
				{
					game.containers[0].addBox(boxes[i]);
				}
				
				this.addOnce1 = true;
			}

			// move boxes 0, 1 and 2 to container 1
			for(var i = 0; i < 3; i++)
			{
				// interpolate box position
				var newPos = game.containers[1].position;
				var oldPos = game.freeBoxes[i].getPosition();
				var x = Math.ceil(oldPos.x + (newPos.x - oldPos.x) * (t / 1000));
				var y = Math.ceil(oldPos.y + (newPos.y - oldPos.y) * (t / 1000));

				// move box
				game.freeBoxes[i].setPosition(new Point(x, y));
			}
		}
		else // MOVE_BOXES_3
		{
			this.addOnce1 = false;

			if(game.freeBoxes.length === 1)
			{
				game.splitBox(0, true, true);
				this.splitOnce = true;
			}
			
			// add boxes 0, 1 and 2 to container 1
			if(!this.addOnce2)
			{
				var boxes = game.freeBoxes.splice(0, 3);
				for(var i = 0; i < boxes.length; i++)
				{
					game.containers[1].addBox(boxes[i]);
				}
				
				this.addOnce2 = true;
			}
			
			// move boxes 0, 1 and 2 to container 2
			for(var i = 0; i < 3; i++)
			{
				// interpolate box position
				var newPos = game.containers[2].position;
				var oldPos = game.freeBoxes[i].getPosition();
				var x = Math.ceil(oldPos.x + (newPos.x - oldPos.x) * (t / 1000));
				var y = Math.ceil(oldPos.y + (newPos.y - oldPos.y) * (t / 1000));

				// move box
				game.freeBoxes[i].setPosition(new Point(x, y));
			}
		}

		// fade to black in 5 seconds
		if(time > 55000)
		{
			var a = ((time - 55000) / 5000);
			context.fillStyle = "rgba(0, 0, 0, " + a + ")";
			context.fillRect(0, 0, canvas.width, canvas.height);
		}

		context.fillStyle = "rgba(0, 255, 0, 1)";
		context.centerFillText("Congratulations!", canvas.height / 4, canvas.width);
	};

	/**
	 * Draw animation frame if we are animating.
	 */
	
	this.draw = function()
	{
		if(!this.animating) return false;
		
		var now = (new Date()).getTime();
		
		if(this.startTime + this.duration > now)
		{
			this.drawFrame(now - this.startTime);
			return true;
		}
		else
		{
			// draw last frame
			this.drawFrame(this.duration);
			return false;
		}
		
	};
}
