/**
 * The explanation center is called from the message center. Whenever the message center has nothing more to say, and
 * hasn't said anything for a while, it calls the explanation center to explain something. The explanation center goes
 * through its list of explanations from top to bottom, to find one that is useful, and prints the message in the
 * message center area on the screen. To find out when an explanation is 'useful', see the comments for
 * explainSomething.
 * 
 * The explanations in the list (found at the bottom of this class) have the following properties:
 * 
 * name						The name of the explanation, equal to the action is explains (see below for list)
 * applicable				Whether or not this explanation is applicable at the moment (in this level, for example)
 * explained				Whether or not this has ever been explained to the user
 * explainedThisRound		Whether or not this has been explained to the user this round
 * done						Whether or not the user has ever performed the associated action
 * message					The message used to explain the action
 * actionMakesApplicable	When the action for this explanation is done, these other explanations become applicable
 * obvious					Means it doesn't need to be explained if the user already did it
 * 
 * The following actions are supported (explained) by the explanation center:
 * 
 * singleBoxSelection
 * boxDragging
 * droppingInContainer
 * takeBoxOutOfContainer
 * 
 * Events are fired by Input describe when some of these actions are done. These events all come directly into the
 * explanation center's handleEvent function. Some others are translated using states by the message center, which calls
 * the setDone function directly. More details about when they are fired can be found in Input and Sates.
 */
function ExplanationCenter()
{
	/**
	 * Checks if there's something to explain, explains it and returns true in that case.
	 *
	 * @returns {Boolean} True if something was explained, false otherwise.
	 */
	this.explainSomething = function()
	{
		/* Find the first thing in the array of explanations that is applicable here, has not been explained before and
		 * has not been done by the user. If something is found, print the explanation and return true. */
		for(var i = 0; i < this.explanations.length; i++)
		{
			var explanation = this.explanations[i];
			if(explanation.applicable && !explanation.explained && !explanation.done)
			{
				printMessage(explanation.message);
				explanation.explained = true;
				explanation.explainedThisRound = true;
				return true;
			}
		}
		/* If the user has never done something, even though we explained it before, explain it again. Only once per
		 * round of course. Print and return true. */
		for(var i = 0; i < this.explanations.length; i++)
		{
			var explanation = this.explanations[i];
			if(explanation.applicable && !explanation.done && !explanation.explainedThisRound)
			{
				printMessage(explanation.message);
				explanation.explained = true;
				explanation.explainedThisRound = true;
				return true;
			}
		}
		/* Maybe the user did something, but we have never explained how to do it. They may not have understood what
		 * they were doing, so explain it here. Print and return true. */
		for(var i = 0; i < this.explanations.length; i++)
		{
			var explanation = this.explanations[i];
			if(explanation.applicable && explanation.done && !explanation.explained && !explanation.obvious)
			{
				printMessage(explanation.message);
				explanation.explained = true;
				explanation.explainedThisRound = true;
				return true;
			}
		}
		/* Nothing to explain */
		return false;
	};


	/**
	 * Inform the explanation center that a new round was started. This means all previous explanations may now be
	 * re-explained if deemed necessary.
	 *
	 * @returns Nothing
	 */
	this.newRound = function()
	{
		for(var i = 0; i < this.explanations.length; i++)
		{
			this.explanations[i].explainedThisRound = false;
		}
	};


	/**
	 * Something happened. If it means the user did something, see if it's something that has an explanation for how to
	 * do it. Obviously the user figured it out, so set 'done' to true. Leave 'explained' to false though in case it was
	 * an accidental and nonrepeatable discovery.
	 *
	 * @param {String} event	Description (one word, see comment above class) of the event that took place
	 *
	 * @returns Nothing
	 */
	this.handleEvent = function(event)
	{
		// This function turned out to be a bit overkill, but it's being used so leave it in
		this.setDone(event);
	};


	/**
	 * Mark the explanation for the action as done.
	 *
	 * @param {String} action	The name of the action
	 * @returns Nothing
	 */
	this.setExplained = function(action)
	{
		var explanation = this.findExplanation(action);
		if(explanation)
		{
			explanation.explained = true;
			explanation.explainedThisRound = true;
		}
	};


	/**
	 * Mark the action for action as done. This does not mark the explanation done, just the action itself.
	 *
	 * @param {String} action	The name of the action
	 * @returns Nothing
	 */
	this.setDone = function(action)
	{
		var explanation = this.findExplanation(action);
		if(explanation)
		{
			explanation.done = true;
			if(explanation.actionMakesApplicable)
			{
				/* Find the explanations that become applicable after this action was taken */
				for(var i = 0; i < explanation.actionMakesApplicable.length; i++)
				{
					var newlyApplicable = this.findExplanation(explanation.actionMakesApplicable[i]);
					if(newlyApplicable) newlyApplicable.applicable = true;
				}
			}
		}
	};


	/**
	 * Mark the explanation for action as applicable.
	 *
	 * @param {String} action	The name of the action
	 * @returns Nothing
	 */
	this.setApplicable = function(action)
	{
		var explanation = this.findExplanation(action);
		if(explanation) explanation.applicable = true;
	};


	/**************************************************** PRIVATE *****************************************************/


	/**
	 * Tries to find the explanation by the given name in the list of explanations and returns it, or null if it's not
	 * found.
	 *
	 * @param {String} name	The name of the action to which the explanation belongs
	 * @returns {Object}	An 'Explanation object' (see this.explanations) if it's found, null otherwise.
	 */
	this.findExplanation = function(name)
	{
		for(var i = 0; i < this.explanations.length; i++)
		{
			if(this.explanations[i].name === name) return this.explanations[i];
		}
		return null;
	};


	/* This is an array to ensure enumeration order */
	this.explanations =
	[
		{name: 'singleBoxSelection', applicable: true, explained: false, explainedThisRound: false, done: false,
				message: 'ESSingleBoxSelection', obvious: true},
		{name: 'boxDragging', applicable: true, explained: false, explainedThisRound: false, done: false,
				message: 'ESBoxDragging', obvious: true},
		{name: 'droppingInContainer', applicable: true, explained: false, explainedThisRound: false, done: false,
				message: 'ESDroppingInContainer', actionMakesApplicable: ['takeBoxOutOfContainer'], obvious: true},
		{name: 'takeBoxOutOfContainer', applicable: false, explained: false, explainedThisRound: false, done: false,
				message: 'ESTakeBoxOutOfContainer', obvious: true},
		{name: 'depth1BoxSplit', applicable: false, explained: false, explainedThisRound: false, done: false,
				message: 'ESSplitDepth1Box', obvious: true}
	];
}