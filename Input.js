/**
 * The Input class handles all mouse and keyboard events. The functions in this class are bound to those events by
 * statements in game.js. Any actions related to these events, such as the (de)selection of boxes, adding and removing
 * boxes to and from containers etc are started from here.
 * 
 * High level events such as box selection are fired from here to the messag ecenter, and then to the explanation center
 * and the states from there. The following events are generated:
 * 
 * boxSelected			-	When a box is added to a box group anywhere in any way
 * boxAddedToContainer	-	When a box group is added to a container (fired once per group)
 * boxDragging			-	When a box group is dragged. Fired every frame.
 * depthxBoxSplit		-	When a box of depth x is split
 * 
 * @param {Game} game
 * @returns {Input}	A new Input instance connected to game
 */
function Input(game)
{
	this.game = game;
	this.enabled = true;

	/**
	 * Checks if there is a box at the current cursor location, and selects it. The top box is always selected. Free
	 * boxes are on top of boxes in containers. If a box is selected, it is added to the current game.multiSelectGroup.
	 * If it is the first box, this box group is created.
	 *
	 * @param {Event} e
	 * @returns {Boolean} True if a box was selected, false otherwise
	 */
	this.select = function(e)
	{
		var localCursor = findLocalCursor(e);
		
		if(!game.multiSelectGroup) game.multiSelectGroup = new BoxGroup();
		
		/* Check if there's a box here in free space. If so, remove it from its place, add it to multiSelectGroup and
		 * return true. Iterate from back to front to make sure that if two boxes are on top of one another, the top one
		 * is selected */
		for(var i = game.freeBoxes.length - 1; i >= 0; i--)
		{
			if(game.freeBoxes[i].withinBounds(localCursor))
			{
				game.freeBoxes[i].highlight();
				game.multiSelectGroup.push(game.freeBoxes.splice(i, 1)[0]);
				game.multiSelectGroup.selectBox(localCursor);
				game.movingSelection = true;
				messageCenter.handleEvent('boxSelected');
				return true;
			}
		}

		/* Check if there's a container below the cursor. If so, check its boxes. If there is a box under the cursor,
		 * add it to the group and return true. If not, return false. */
		for(var i = 0; i < game.containers.length; i++)
		{
			if(game.containers[i].withinBounds(localCursor))
			{
				var boxes = game.containers[i].boxes;

				/* Find box in container that is below cursor, remove it from its place, assign it to
				 * multiSelectGroup and return true. */
				for(var j = 0; j < boxes.length; j++)
				{
					if(boxes[j].withinBounds(localCursor))
					{
						boxes[j].highlight();
						game.multiSelectGroup.push(boxes[j]);
						game.multiSelectGroup.selectBox(localCursor);
						var oldWidth = boxes[j].width;
						// Resize the box to its free size
						boxes[j].width = boxes[j].height = getBoxSize(boxes[j].depth);
						var newWidth = boxes[j].width;
						// Make sure it resizes around the grab point
						boxes[j].setPosition(boxes[j].getPosition().sub(
								game.multiSelectGroup.selectedBoxGrabPoint.mul(newWidth / oldWidth - 1)));
						// Make sure the grabpoint stays in the same relative spot
						game.multiSelectGroup.selectedBoxGrabPoint.mulBang(newWidth / oldWidth);
						game.containers[i].removeBox(j);
						game.movingSelection = true;
						messageCenter.handleEvent('boxSelected');
						return true;
					}
				}
				/* This container is underneath the cursor, but none of its boxes is. No need to check the other
				 * containers */
				return false;
			}
		}

		/* No box here */
		return false;
	};
	
	
	/**
	 * Toggle the selection of the box under the cursor at the time the event was fired. This only applies to free
	 * boxes and boxes in box groups. Boxes inside containers cannot be grabbed this way.
	 * 
	 * @param {Event} e
	 * @returns {Boolean}	True iff a box was swithed from free space to a box group or the other way around
	 */
	this.toggleSelect = function(e)
	{
		var localCursor = findLocalCursor(e);
		
		if(!game.multiSelectGroup) game.multiSelectGroup = new BoxGroup();
		
		// Check whether there's a box here in multiSelectGroup. If so, remove it and add it to free space
		if(game.multiSelectGroup.selectBox(localCursor))
		{
			game.multiSelectGroup.selectedBox.resetColor();
			game.freeBoxes.push(game.multiSelectGroup.selectedBox);
			game.multiSelectGroup.remove(game.multiSelectGroup.selectedBox);
			game.multiSelectGroup.selectedBox = undefined;
			return true;
		}
		
		/* Check if there's a box here in free space. If so, remove it from its place and add it to multiSelectGroup.
		 * Iterate from back to front to make sure that if two boxes are on top of one another, the top one is selected */
		for(var i = game.freeBoxes.length - 1; i >= 0; i--)
		{
			if(game.freeBoxes[i].withinBounds(localCursor))
			{
				game.freeBoxes[i].highlight();
				game.multiSelectGroup.push(game.freeBoxes.splice(i, 1)[0]);
				game.multiSelectGroup.selectBox(localCursor);
				game.movingSelection = false;
				messageCenter.handleEvent('boxSelected');
				return true;
			}
		}
		
		return false;
	};


	/**
	 * Checks if any free boxes are partly within the rubber band, and adds them to game.multiSelectGroup, creating
	 * this BoxGroup if necessary. It also checks all boxes currently in the group, and if they are no longer partly
	 * within the rubber band they are removed from the group and added to the free list.
	 * 
	 * @param {Event} e
	 * @returns nothing
	 */
	this.multiSelect = function(e)
	{
		game.rubberBand.to = findLocalCursor(e);
		if(!game.multiSelectGroup) game.multiSelectGroup = new BoxGroup();

		/* Check if any free boxes are partly within the rubber band and add them */
		for(var i = game.freeBoxes.length - 1; i >= 0; i--)
		{
			if(game.rubberBand.withinBounds(game.freeBoxes[i]))
			{
				game.freeBoxes[i].highlight();
				game.multiSelectGroup.push(game.freeBoxes.splice(i, 1)[0]);
			}
		}
		/* Check if all boxes that are in the group are still in the rubber band */
		// In order to avoid messing up the order of the boxes, run from front to back. It will still happen that if you
		// select and then deselect a box that was below another one, it will now be on top.
		var i = 0;
		while(i !== game.multiSelectGroup.boxes.length)
		{
			var b = game.multiSelectGroup.boxes[i];
			if(!game.rubberBand.withinBounds(b))
			{
				b.resetColor();
				game.freeBoxes.push(game.multiSelectGroup.boxes.splice(i, 1)[0]);
			}
			else i++;
		}
	};


	/**
	 * Drops all selected boxes in the container under the cursor. If there is no container under the cursos, the boxes
	 * over a container are dropped in that container and deselected, while the rest remains selected.
	 * 
	 * @param {Event} e
	 * @returns nothing
	 */
	this.drop = function(e)
	{
		var localCursor = findLocalCursor(e);

		if(game.multiSelectGroup)
		{
			/* Check if the cursor is on top of a container. If so, drop all boxes inside. */
			for(var i = 0; i < game.containers.length; i++)
			{
				if(game.containers[i].withinBounds(localCursor))
				{
					for(var k = 0; k < game.multiSelectGroup.boxes.length; k++)
					{
						game.multiSelectGroup.boxes[k].resetColor();
						game.containers[i].addBox(game.multiSelectGroup.boxes[k]);
						messageCenter.handleEvent('boxAddedToContainer');
					}
					
					game.multiSelectGroup = undefined;
					game.movingSelection = false;
					/* The boxes were added to the container. This is always the last action before a player can win */
					if(game.checkVictoryCondition())
					{
						game.loadNewLevel(true);
					}
					return;
				}
				/* Check the position of all boxes. Boxes which are mostly hovering over a container, will drop inside
				 * and no longer be selected. */
				for(var k = 0; k < game.multiSelectGroup.boxes.length; k++)
				{
					if(game.containers[i].captures(game.multiSelectGroup.boxes[k]))
					{
						game.multiSelectGroup.boxes[k].resetColor();
						game.containers[i].addBox(game.multiSelectGroup.boxes[k]);
						game.multiSelectGroup.removeAt(k);
						messageCenter.handleEvent('boxAddedToContainer');
						/* The boxes were added to the container. This is always the last action before a player can win */
						if(game.checkVictoryCondition())
						{
							game.loadNewLevel(true);
						}
					}
				}
			}
			
			game.movingSelection = false;
		}
	};


	/**
	 * Handles mouse movements. In case a boxgroup is selected, move the boxgroup with the cursor. If doing rubber band
	 * selection, change the selection.
	 *
	 * @param {Event} e
	 * @returns nothing
	 */
	this.handleMouseMove = function(e)
	{
		if(!input.enabled) return;
		
		var localCursor = findLocalCursor(e);
		
		if(game.movingSelection)
		{
			messageCenter.handleEvent('boxDragging');
			game.multiSelectGroup.move(localCursor);
		}
		else if(game.rubberBand)
		{
			game.rubberBand.to = localCursor;
			input.multiSelect(e);
		}
	};


   /**
	* Handles mouse down event. If the click is outside the canvas, return. If the splash is active, kill it and return.
	* If the ctrl key is clicked, toggle the selection of the top box under the cursor and return. Then set the click
	* position in game.positionOnDown. If a box group is active and a user clicked a box, start moving the group and
	* return (do mind that if the user does mouse up before moving the cursor, something else will happen. See
	* handleMouseUp). If the mouse down is outside the group, deselect all boxes. If there is a box here, select it.
	* Finally, if none of that applies, start a rubber band selection at this point.
	*
	* @param {Event} e
	*
	* @returns nothing
	*/
	this.handleMouseDown = function(e)
	{
		if(!input.enabled) return;
		
		var localCursor = findLocalCursor(e);
		if(!withinCanvas(localCursor)) return;
		
		if(game.splash) 
		{
			game.splash = undefined;
			return;
		}
		
		if(e.ctrlKey)
		{
			input.toggleSelect(e);
			return;
		}
		
		game.positionOnDown.x = localCursor.x;
		game.positionOnDown.y = localCursor.y;
		
		// Multiselect group active?
		if(game.multiSelectGroup)
		{
			// Clicked within one of the boxes?
			if(game.multiSelectGroup.selectBox(localCursor))
			{
				/* If the group is already moving, this is the special case of one box having been selected by clicking
				 * (down and up) on it. Drop it here. */
				if(game.movingSelection) input.drop(e);
				/* Else, start moving this group */
				else game.movingSelection = true;
				return;
			}
			else
			{
				game.destroyMultiSelectGroup();
			}
		}

		// Box here to select?
		if(input.select(e))
		{
			game.movingSelection = true;
		}
		else
		{
			// Nope, start rubber band selection
			game.rubberBand = new RubberBand(game.positionOnDown);
		}
	};


   /**
    * Handles mouse up event. If a box group is selected, and the cursor position is different from positionOnDown, drop
	* all boxes. If the cursor is still in the same place above the group, select only the top box under the cursor.
	* Destroy the rubber band.
	*
	* @param {Event} e
	* 
	* @returns nothing
    */
	this.handleMouseUp = function(e)
	{
		if(!input.enabled) return;
		
		var localCursor = findLocalCursor(e);
		
		if(game.movingSelection)
		{
			// if mouse moved since down, drop selection
			if(localCursor.x !== game.positionOnDown.x || localCursor.y !== game.positionOnDown.y)
			{
				input.drop(e);
			}
			else
			{
				game.destroyMultiSelectGroup();
				input.select(e);
			}
		}

		endGame.maybeShowButton();

		game.rubberBand = undefined;
	};

	/**
	 * First destroys any box group, moving the boxes to the free list. Then checks for the top box under the cursor. If
	 * one is found, an attempt is made to split it in ten.
	 * 
	 * @param {Event} e
	 * @returns nothing
	 */
	this.handleDoubleClick = function(e)
	{
		if(!input.enabled) return;
		
		var localCursor = findLocalCursor(e);
		game.destroyMultiSelectGroup();
		/* First check the free boxes */
		for(var i = game.freeBoxes.length - 1; i >= 0; i--)
		{
			if(game.freeBoxes[i].withinBounds(localCursor))
			{
				/* Only splits the box if splitting a box at this depth even more at this difficulty is allowed */
				game.splitBox(i, true);
				// Only the top box here can be split
				return;
			}
		}
		/* Then the boxes in containers */
		for(var i = 0; i < game.containers.length; i++)
		{
			if(game.containers[i].withinBounds(localCursor))
			{
				for(var k = 0; k < game.containers[i].boxes.length; k++)
				{
					if(game.containers[i].boxes[k].withinBounds(localCursor))
					{
						var box = game.containers[i].boxes[k];
						game.containers[i].removeBox(k);
						game.freeBoxes.push(box);
						game.splitBox(game.freeBoxes.length - 1, true);
					}
				}
			}
		}
	};
}
