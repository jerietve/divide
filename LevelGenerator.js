/**
 * A level factory with persistent settings. A description of the difficulties follows:
 *
 * Difficulty 1
 * The same amount of boxes as containers. Simply drag every box to a different container.
 *
 * Difficulty 2
 * The boxes can be evenly divided amongst the containers. Containers contain more than one box in the final state.
 *
 * Difficulty 3
 * The boxes must be split once. Division requires one decimal.
 *
 * Difficulty 4
 * The boxes must be split twice. Division requires two decimals.
 *
 * Difficulty 5
 * The boxes must be split more than twice. Divisision results in more than two decimals.
 *
 *
 * @constructor
 * @param	{Number}	difficulty		The difficulty of newly generated levels
 * @param	{Number}	minContainers	The minimum amount of containers in a newly generated level
 * @param	{Number}	maxContainers	The maximum amount of containers in a newly generated level
 */
function LevelGenerator(difficulty, minContainers, maxContainers)
{
	this.difficulty = difficulty;

	/**
	* Generates a new Level of the current difficulty.
	*
	* @param	{Level}	unlike	The new level will not be equal to this level
	*
	* @returns {Level}	A new Level object, or null in case the requested difficulty is not supported or the parameters
	*					make creation of such a level impossible.
	*/
	this.generateLevel = function(unlike)
	{
		var level = new Level();

		if(!this.options[this.difficulty])
		{
			this.generateOptions(this.difficulty);
		}

		/* Pick an option  */
		var random = Math.random() * this.options[this.difficulty].length;
		var chosen = Math.floor(random);
		/* Make sure this won't result in the level we're not supposed to make */
		if(unlike && unlike.difficulty === this.difficulty
				&& unlike.dividend === this.options[this.difficulty][chosen]["dividend"]
				&& unlike.divisor === this.options[this.difficulty][chosen]["divisor"])
		{
			/* Try to select the next option, or if it doesn't exist then.. well.. too bad */
			chosen = (chosen + 1) % this.options[this.difficulty].length;
		}
		level.difficulty = this.difficulty;
		level.dividend = this.options[this.difficulty][chosen]["dividend"];
		level.divisor = this.options[this.difficulty][chosen]["divisor"];
		level.mission = retrieveString('levelDescription', {'number1' : level.dividend, 'number2' : level.divisor});
		return level;
	};


	/**
	 * Sets the minimum amount of containers for the next level to be generated
	 *
	 * @param	{int}	minContainers	New minimum amount of containers
	 * @returns nothing
	 */
	this.setMinContainers = function(minContainers)
	{
		this.minContainers = minContainers;
		//reset the options array, everything must be recalculated
		this.options = new Array();
	};


	/**
	 * Sets the maximum amount of containers for the next level to be generated
	 *
	 * @param	{int}	maxContainers	New maximum amount of containers
	 * @returns nothing
	 */
	this.setMaxContainers = function(maxContainers)
	{
		this.maxContainers = maxContainers;
		//reset the options array, everything must be recalculated
		this.options = new Array();
	};
	
	
	/**
	 * Development function. Prints all containers: boxes (divisor: dividend) combinations to the console for the given
	 * properties.
	 * 
	 * @param	{int}	minContainers	The minimum number of containers
	 * @param	{int}	maxContainers	The maximum number of containers
	 * @param	{int[]}	difficulties	Optional, the difficulties for which to print. Default is all.
	 * 
	 * @returns nothing
	 */
	this.printOptions = function(minContainers, maxContainers, difficulties)
	{
		var originalDifficulty = this.difficulty;
		var originalMinContainers = this.minContainers;
		var originalMaxContainers = this.maxContainers;
		this.minContainers = minContainers;
		this.maxContainers = maxContainers;
		this.options = new Array();
		
		difficulties = difficulties || [1, 2, 3, 4, 5];
		var printString = "";
		var numOptions = 0;
		for(var i = 0; i < difficulties.length; i++)
		{
			if(i !== 0) printString += "\n";
			this.difficulty = difficulties[i];
			this.generateOptions();
			printString += 'Difficulty ' + difficulties[i] + ':';
			
			var divisors = [];
			for(var j = 0; j <= 10; j++)
			{
				divisors.push([]);
			}
			for(var j = 0; j < this.options[difficulties[i]].length; j++)
			{
				var divisor = this.options[difficulties[i]][j]['divisor'];
				divisors[divisor].push(this.options[difficulties[i]][j]['dividend']);
				numOptions++;
			}
			for(var j = 0; j < divisors.length; j++)
			{
				if(divisors[j].length === 0) continue;
				printString += "\n" + j + ': ';
				for(var k = 0; k < divisors[j].length; k++)
				{
					if(k !== 0) printString += ', ';
					printString += divisors[j][k];
				}
			}
		}
		
		console.log('There are ' + numOptions + ' options')
		console.log(printString);
		this.difficulty = originalDifficulty;
		this.minContainers = originalMinContainers;
		this.maxContainers = originalMaxContainers;
		this.options = new Array();
	};


	/**************************************************** PRIVATE *****************************************************/
	
	
	this.minContainers = minContainers;
	this.maxContainers = maxContainers;
	/* This array contains arrays of all options (combinations of dividend and divisor) per difficulty which can be
	 * chosen from. Changing the minimum or maximum number of containers resets the array. Options for different
	 * difficulties are added dynamically as needed. */
	this.options = new Array();


	/**
	 * Calculates the options for the current difficulty and min/max containers and sets the options array
	 * appropriately.
	 *
	 * @private
	 * @returns nothing
	 */
	this.generateOptions = function()
	{
		this.options[this.difficulty] = new Array();

		switch(this.difficulty)
		{
			case 1:
				for(var i = this.minContainers; i <= this.maxContainers; i++)
				{
					this.options[this.difficulty].push({"dividend": i, "divisor": i});
				}
				break;
			case 2:
			case 3:
			case 4:
				var step = Math.pow(10, this.difficulty - 2);
				for(var dividend = step; dividend <= 10 * step; dividend += step)
				{
					divisorLoop:
					for(var divisor = 1; divisor <= 10; divisor++)
					{
						/* Weed out difficult 1 */
						if(divisor * step === dividend) continue;
						/* Weed out difficulties 2, 3 and 4 if necessary */
						for(var i = 10; i <= step; i *= 10) if((dividend / i) % divisor === 0) continue divisorLoop;
						/* Check the amount of decimals is exactly that required. Or if it's level five, just add it. */
						if((this.difficulty === 5 || dividend % divisor === 0) && divisor >= this.minContainers &&
								divisor <= this.maxContainers)
						{
							this.options[this.difficulty].push({"dividend": dividend / step, "divisor": divisor});
						}
					}
				}
				break;
			case 5:
				this.options[this.difficulty].push({"dividend": 1, "divisor": 3});
		}
	};
}


/**
 * Do not directly make Level objects, but use the generateLevel function.
 *
 * @prop	{Number}	difficulty	The difficulty of the level
 * @prop	{Number}	dividend	The number being divided in this level, the amount of boxes
 * @prop	{Number}	divisor		The number dividing the dividend in this level, the amount of containers
 * @prop	{String}	mission		A description of the mission to complete this round
 */
function Level()
{
	this.difficulty;
	this.dividend;
	this.divisor;
	this.mission;


	/**
	 * Levels are equal if they have the same difficulty, dividend and divisor. 
	 * 
	 * @param {Level} otherLevel	The level to compare to this one
	 * @returns {Boolean} true iff the levels are equal
	 */
	this.equals = function(otherLevel)
	{
		return this.difficulty === otherLevel.difficulty && this.dividend === otherLevel.dividend
				&& this.divisor === otherLevel.divisor;
	};
}
