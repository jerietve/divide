function MessageCenter()
{
	// If a user doesn't invoke a message within this amount of seconds, the explanation center will try to help out
	this.WAIT_BEFORE_EXPLAINING = 15;

	this.currentState;
	this.explanationCenter;
	this.lastMessageTime;

	/**
	 * Starts this message center in its start state. This state is the very first state of a new game for a player who
	 * has never played before.
	 *
	 * @returns Nothing
	 */
	this.init = function()
	{
		this.currentState = new StartState();
		this.explanationCenter = new ExplanationCenter();
		this.lastMessageTime = Date.now();
	};


	/**
	 * Makes sure whatever the action should be taken, in case of the provided event in the current state, it taken.
	 *
	 * @param {String} event	Description of the event that needs to be handled
	 * @returns Nothing
	 */
	this.handleEvent = function(event)
	{
		if(event === 'difficultyJump')	this.loadState(this.difficultyEntryStates[game.levelGenerator.difficulty]());
		else if(this.currentState.hasOwnProperty(event))
		{
			var actions = this.currentState[event]();
			for(var i = 0; i < actions.length; i++)
			{
				this.takeAction(actions[i]);
			}
		}
		else if(this.currentState.hasOwnProperty('handleEvent')) this.currentState.handleEvent(event);
		this.explanationCenter.handleEvent(event);
	};


	/**
	 * Checks the first timer of the currently active state, and takes its action if is has expired. Then the next timer
	 * is started if it exists.
	 *
	 * @returns Nothing
	 */
	this.checkTimers = function()
	{
		if(this.currentState.timerActions.length > 0 && this.currentState.timerActions[0]['timer'].hasExpired())
		{
			var timerAction = this.currentState.timerActions[0];
			/* Run the function in the timer action and save its result */
			var actions = timerAction['actions']();
			for(var a = 0; a < actions.length; a++)
			{
				this.takeAction(actions[a]);
				/* Escape from this function if a new state has since been loaded, to avoid deleting its timers */
				if(actions[a]['action'] === 'load state') return;
			}
			/* Delete the timer action */
			this.currentState.timerActions.splice(0, 1);
			/* Start the next timer if it exists */
			this.currentState.startTimer();
		}
		/* If it's been too long, see if something might need to be explained */
		if(!this.currentState.hasRunningTimers() && itsBeen(this.WAIT_BEFORE_EXPLAINING, this.lastMessageTime))
		{
			if(this.explanationCenter.explainSomething()) this.lastMessageTime = Date.now();
		}
	};


	/**
	 * Checks the 'action' key of the array and provides the 'param' key to the appropriate function.
	 *
	 * @param {Array} action	{{action: String, param: Object}+}
	 * @returns Nothing
	 */
	this.takeAction = function(action)
	{
		switch(action['action'])
		{
			case 'show message':
				printMessage(action['param'], action['options']);
				this.lastMessageTime = Date.now();
				break;
			case 'load state':
				this.loadState(action['param']);
				break;
			case 'set explained':
				this.explanationCenter.setExplained(action['param']);
				break;
			case 'set done':
				this.explanationCenter.setDone(action['param']);
				break;
		}
	};


	/**
	 * Set the new state, starting its timers and printing its start message.
	 *
	 * @param {State} state	The state to load
	 * @returns Nothing
	 */
	this.loadState = function(state)
	{
		this.currentState = state;
		this.currentState.startTimer();
		if(state.message)
		{
			printMessage(state.message, state.messageOptions);
			this.lastMessageTime = Date.now();
		}
	};


	/**************************************************** PRIVATE *****************************************************/
	
	
	this.currentState;	
	/* Functions returning the state to be loaded when jumping to a certain difficulty. Better hope these states don't
	 * rely on values from the previous state, which may be anything. */
	this.difficultyEntryStates = {1: function(){ return new TryMoveBoxesState();},
			2: function(){ return new EnterDifficulty2State();},
			3: function(){ return new EnterDifficulty3State();},
			4: function(){ return new EnterDifficulty4State();},
			5: function(){ return new EnterDifficulty5State();}};
}