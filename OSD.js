function OSD(game)
{
	this.boxes = [];
	
	var tmpBox = new Box();
	for(var i = 0; i < Object.keys(tmpBox.depthColors).length; i++)
	{
		this.boxes[i] = new Box();
		this.boxes[i].setDepth(i);
		this.boxes[i].width = this.boxes[i].height = 20;
	}
	
	this.drawPart = function(index)
	{
		this.x -= context.measureText(this.selected[index]).width;
		context.fillText(this.selected[index], this.x, this.y);
		this.x -= 25;
		this.boxes[index].setPosition(new Point(this.x, this.y - 17));
		this.boxes[index].draw();
		this.x -= 20;
	};
	
	this.draw = function()
	{
		var drawn = false; // set to true when something is actually drawn
		
		function numSelected(depth, boxgroup)
		{
			var counter = 0;
			$.each(boxgroup, function(i, box) {
				if(box.depth === depth) counter++;
			});
			return counter;
		}
		
		if(game.multiSelectGroup)
		{
			this.selected = [];
			for(var i = 0; i < this.boxes.length; i++)
			{
				this.selected[i] = numSelected(i, game.multiSelectGroup.boxes);
			}
		}
		else
		{
			this.selected = undefined;
		}
		
		// margins of 10, right aligned
		this.x = context.canvas.width - 10;
		this.y = 30;
		
		if(this.selected)
		{
			context.font = "20px sans-serif";
			context.fillStyle = "rgba(255, 255, 255, 1)";
			
			for(var i = this.selected.length - 1; i >= 0; i--)
			{
				if(this.selected[i] > 0)
				{
					this.drawPart(i);
					drawn = true;
				}
			}
			
			// only show "Selected" when some boxes are selected
			if(drawn)
			{
				context.fillStyle = "rgba(255, 255, 255, 1)";
				this.x -= context.measureText("Selected:").width;
				context.fillText("Selected:", this.x, this.y);
			}
		}
	};
}
