Divide
======

Divide will teach you how to, well, do division! The current version of the game is basic, but complete. Check out the game in action to see how it works. Select boxes with the mouse and drag them to a container!

User Features
-------------

A user playing the game will experience the following features:

* Intricate selection possibilities: select on click, select while mousedown, rubberband multi-selection, ctrl-click multi selection
* Background music
* Random level generation
* Splash screen
* Extensive AI, commenting on your progress through the game
* Translations into multiple languages

Technical Features
------------------

A developer inspecting the code will see the following features:

* Modern HTML, CSS and Javascript; no Flash, or any other plugins
* Object-oriented program architecture
* jQuery for quick development
* Render to texture for special effects
* localStorage for persistence
* Extensive comments, including JavaDoc style comments on almost every method

Process Features
----------------

The game development process consisted of these features:

* Git and Bitbucket for revision control, collaboration and hosting of public branches
* A game design document listing detailed steps to complete each milestone
