/**
 * The Resource object is filled with properties by the functions below. The properties are taken from xml files in the
 * language directory. The first level of elements within the root element will turn into properties of the resource
 * objects in game called R. These elements' child elements turn into properties of the properties.
 * 
 * @returns {Resource}	An empty resource
 */
function Resource()
{
}


/**
 *
 * @param {String} name		The name of the xml string element containing the messsage to be printed
 * @param {Array} options	The inputs for the string
 * @returns {String}		The string within the xml string element with name name
 */
function retrieveString(name, options)
{
	return completeString(R.strings[name], options);
}


/**
 * Takes a string with formatting labels inside and fills in the information from the options.
 *
 * @param {String} string			The original unformatted string
 * @param {Array<String>} options	Associative array of options 'key' : 'value' where the values should replace the
 *									placeholders with the names of the keys in the string
 * @returns {String}				The formatted string
 */
function completeString(string, options)
{
	/* For all options */
	for(var key in options)
	{
		/* Don't iterate over inherited stuff */
		if(options.hasOwnProperty(key))
		{
			/* Look for the placeholder in the string */
			var startIndex;
			while((startIndex = string.indexOf('%' + key)) !== -1)
			{
				/* If we're deadling with a number and were not told not to spell it out, spell it out */
				var value;
				if(typeof options[key] === "number" && (options.writeOut === undefined || options.writeOut))
				{
					value = toWord(options[key]);
				}
				/* If we're not spelling it out, at least make sure we have a string */
				else value = options[key] + "";
				/* Check if we're in a number-word field */
				if(insideNumberWordField(string, startIndex))
				{
					/* We are. This means the value to be insert must be a number. First remove the {, the % and the
					 * placeholder and insert the number */
					string = string.insert(startIndex - 1, value, key.length + 2);
					/* Check if the number is singular or plural */
					// == because it might be '1'
					if(options[key] == 1)
					{
						/* Make sure all stuff except the number and singular word disappears */
						var removeFrom = string.indexOf('|', startIndex - 1);
						var removeTill = string.indexOf('}', startIndex);
						string = string.insert(removeFrom, '', removeTill - removeFrom + 1);
					}
					else
					{
						/* Make sure all stuff except the number and plural word disappears */
						// First remove the singular word and the |
						var removeFrom = startIndex + value.length;
						var removeTill = string.indexOf('|', startIndex - 1);
						string = string.insert(removeFrom, '', removeTill - removeFrom + 1);
						// Then remove the } closing the field
						var braceLocation = string.indexOf('}', startIndex);
						string = string.insert(braceLocation, '', 1);
					}
				}
				else
				{
					/* Just replace the placeholder */
					string = string.insert(startIndex, value, key.length + 1);
				}
			}
		}
	}

	return string;
}


/**
 * Checks if the index is within a {%number singular|plural} field. It does this by simply going left and looking for
 * the first {.
 *
 * @param {String} string		The strng to check inside
 * @param {Number} startIndex	The index at which to check
 * @returns {Boolean}	True if the index is inside such a field, false otherwise
 */
function insideNumberWordField(string, startIndex)
{
	return string.lastIndexOf('{', startIndex) > string.lastIndexOf('}', startIndex);
}


/**
 * Creates the R object, filling it with the strings of the file that belongs with the language.
 *
 * @param {String} language	A value such as 'nl' or 'ko' to get the language file (at resourcePath/language.xml)
 * @returns Nothing
 */
function createResource(language)
{
	R = R || new Resource();
	if(language === 'en') loadLanguages([language]);
	else loadLanguages(['en', language]);
};


/**
 * Loads all languages in the array into R in order from left to right, overwriting any values found by ones found
 * later. It calls itself repeatedly after all closures have completed running, finally callint init() after all
 * languages have been loaded.
 *
 * @param {String Array} languages	Entries such as 'en' and 'nl'
 * @returns {Nothing}
 */
function loadLanguages(languages)
{
	/* Get the file for the first language */
	$.get(RESOURCE_PATH + languages[0] + '.xml', function(d)
	{
		/* The loading of the resources will happen through functions which are split off from the main 'thread' and all
		 * run concurrently. We need a way to know when the last function is done, because exactly then init() should be
		 * called. Count the total number of resources that we're going to load here. */
		var resourceLoadCounter = $(d).children().first().children().children().size();
		/* Go over all children of the root (first stepping into the root) */
		$(d).children().first().children().each(function()
		{
			/* Find the tagname of this element */
			var tagName = $(this).prop('tagName');
			/* Create a property with this name if it doesn't exist yet */
			if(!R.hasOwnProperty(tagName)) R[tagName] = new Object();
			/* Read all subelements */
			$(this).children().each(function()
			{
				/* Get the name */
				var name = $(this).attr('name');
				/* Create a property with that name on the parent property just created, overwriting it if it already
				 * exists. */
				R[tagName][name] = $(this).text();

				/* Decrement the amount of resources that still need to load */
				resourceLoadCounter--;

				/* If all resources have been loaded for this language, load the next language. If this was the last
				 * language, initialize the game. There is no danger of this happening twice (because this thread would
				 * have been paused right after decrementing the counter to 1, while in the mean time another thread
				 * decremented it to 0) because JavaScript doesn't pause execution midway any code. Unless events and
				 * browsers etc. Luckily that's not what we're dealing with here. */
				if(resourceLoadCounter === 0)
				{
					languages.splice(0, 1);
					if(languages.length > 0) loadLanguages(languages);
					else init();
				}
			});
		});
	});
}


/**
 * Number words are looked up in R, while unavailable numbers are simply returned.
 *
 * @param {Number} number	The number to convert.
 * @returns {String}	The result (always a string)
 */
function toWord(number)
{
	return R.strings["number" + number] || number + "";
}


/**
 * Takes a number and a plural word, with optionally its singular form, and returns a string that takes into account the
 * number when choosing the plural or singular form. The singular form is automatically derived from the plural form in
 * most cases, but for some special cases you may want to provide it. The reason you have to provide the plural is that
 * the singular form occurs a lot less in general, so any errors in conversion will present themselves less often.
 *
 * @param {Number} number	The number of 'word'
 * @param {String} plural	The plural form of the word
 * @param {Array} options	printOne: boolean; whether to print the word 'one'
 *							singular: String; the singular form of the word (provide this for very strange words)
 * @returns {String}		The number and word concatenated with a space in between
 */
function pluralize(number, plural, options)
{
	if(number !== 1) return toWord(number) + ' ' + plural;
	else if(!options || !options['singular'])
	{
		/* Convert the plural form to singular */
		if(plural.indexOf('es', plural.length - 2) !== -1) singular = plural.substring(0, plural.length - 2);
		else if(plural.charAt(plural.length - 1) === 's') singular = plural.substring(0, plural.length - 1);
		else
		{
			/* The best guess in this case is that singular and plural must be the same */
			singular = plural;
		}
	}
	if(!options || options['printOne'] === true) return 'one ' + singular;
	else return singular;
}