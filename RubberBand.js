function RubberBand(point)
{
	this.from = point;
	this.to = point;
	this.lineWidth = 2;
	this.lineColor;
	this.strokeStyle = "rgba(255, 255, 255, 1)";
	this.lineCap = "round";
	this.lineJoin = "round";


	this.withinBounds = function(box)
	{
		var corners = box.getCorners();
		return rectanglesIntersect(corners[0], corners[2],
				new Point(Math.min(this.from.x, this.to.x), Math.min(this.from.y, this.to.y)),
				new Point(Math.max(this.from.x, this.to.x), Math.max(this.from.y, this.to.y)));
	};


	this.draw = function()
	{
		/* Do not draw a rubber band of one pixel */
		if(this.from.equals(this.to)) return;

		var c = context;
		if(c === undefined)
		{
			throw Error("Box.draw(): No context defined!");
		}

		c.strokeStyle = this.strokeStyle;
		c.lineWidth = this.lineWidth;
		c.lineCap = this.lineCap;
		c.lineJoin = this.lineJoin;
		c.strokeRect(
				this.from.x + this.lineWidth/2,
				this.from.y + this.lineWidth/2,
				this.to.sub(this.from).x - this.lineWidth,
				this.to.sub(this.from).y - this.lineWidth);
	};
}
