function Splash(width, height, level, round, missionStatement)
{
	this.buffer = document.createElement('canvas');
	this.buffer.width = width;
	this.buffer.height = height;
	this.startTime = (new Date()).getTime();
	this.duration = 8000; // in ms
	this.vectors;
	
	this.level = level;
	this.round = round;
	this.missionStatement = missionStatement;
	
	// draw splash to buffer
	
	var bc = this.buffer.getContext('2d');
	bc.fillStyle = "rgba(64, 64, 64, 1)";
	bc.fillRect(0, 0, this.buffer.width, this.buffer.height);
	
	for(var i = 0; i < 4; i++)
	{
		for(var j = 0; j < 6; j++)
		{
			var x = Math.floor(j * (this.buffer.width / 6));
			var y = Math.floor(i * (this.buffer.height / 4));
			var width = Math.ceil(this.buffer.width / 6);
			var height = Math.ceil(this.buffer.height / 4);
			bc.fillStyle = "rgba("+randInt(0, 150)+", "+randInt(0, 150)+", "+randInt(0, 150)+", 1)";
			bc.fillRect(x, y, width, height);
		}
	}
	
	// heights for 1: title, 2: level/round, 3: mission statement
	var drawHeight = [
			(this.buffer.height - 300) / 3 + 100,
			(this.buffer.height - 300) / 3 * 2 + 100,
			(this.buffer.height - 300) / 3 * 3 + 100
	];
	
	// Title
	bc.fillStyle = "rgba(255, 255, 255, 1)";
	bc.font = "100px sans-serif";
	bc.centerFillText("DIVIDE!", drawHeight[0], this.buffer.width);
	
	// Level / round
	var levelRoundText = "Level: " + this.level + " | Round: " + this.round;
	bc.font = "50px sans-serif";
	bc.centerFillText(levelRoundText, drawHeight[1], this.buffer.width);
	
	// Mission Statement
	
	bc.font = "60px sans-serif";
	bc.centerFillText(this.missionStatement, drawHeight[2], this.buffer.width);
	
	
	speak(levelRoundText + ". " + this.missionStatement);

	// initialize vectors

	this.vectors = Array();

	for(var i = 0; i < 4; i++)
	{
		this.vectors[i] = Array();

		for(var j = 0; j < 6; j++)
		{
			this.vectors[i][j] = { rotation: Math.random() * 2 * Math.PI, speed: Math.random() + 1 };
		}
	}
	
	
	/**
	 * Draws this splash if its duration hasn't passed since it was started.
	 * 
	 * @returns {Boolean}	true if the splash was drawn, false if it has expired
	 */
	this.draw  = function()
	{
		var now = (new Date()).getTime();
		if(this.startTime + this.duration > now)
		{
			this.drawFrame(now - this.startTime);
			return true;
		}
		else
		{
			return false;
		}
	};

	/*
		Get animation frame at time milliseconds
	*/

	this.drawFrame = function(time)
	{
		var c = context;
		var t = 0;
		var t2 = 0;
		var ratio = 0;
		
		// first second fades in
		if(time < 1000)
		{
			t2 = Math.min(1 - time / 1000, 1);
		}
		// everything between: show splash
		else if(time < this.duration - 2000)
		{
			t = 0;
		}
		// last two seconds: explosion!
		else
		{
			ratio = (time - this.duration + 1000) / 2000;
			t = ratio * this.buffer.width / 2;
		}

		for(var i = 0; i < this.vectors.length; i++)
		{
			for(var j = 0; j < this.vectors[i].length; j++)
			{
				var x = Math.floor(j * (this.buffer.width / this.vectors[i].length));
				var y = Math.floor(i * (this.buffer.height / this.vectors.length));
				var width = Math.ceil(this.buffer.width / this.vectors[i].length);
				var height = Math.ceil(this.buffer.height / this.vectors.length);
				
				var cx = Math.floor(j * (c.canvas.width / this.vectors[i].length));
				var cy = Math.floor(i * (c.canvas.height / this.vectors.length));
				var cwidth = Math.ceil(c.canvas.width / this.vectors[i].length);
				var cheight = Math.ceil(c.canvas.height / this.vectors.length);
				
				c.save();
				if(t > 0)
				{
					c.rotate(this.vectors[i][j].rotation);
					c.translate(t * this.vectors[i][j].speed, 0);
					c.rotate(-this.vectors[i][j].rotation);
				}
				
				c.translate(cx + cwidth/2, cy + cheight/2);
				if(t > 0) c.rotate(this.vectors[i][j].rotation*t*0.001);
				c.translate(-cwidth/2, -cheight/2);
				
				c.globalAlpha = 1 - ratio * 2;
				c.drawImage(this.buffer, x, y, width, height, 0, 0, cwidth, cheight);
				c.restore();
			}
		}

		c.fillStyle = "rgba(0, 0, 0, "+t2+")";
		c.fillRect(0, 0, this.buffer.width, this.buffer.height);
	};
}
