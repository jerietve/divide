/* The states
 *
 * States are all defined here. The message center is always in a certain state. This state depends on
 * actions the user takes. States have certain events they respond to and certain actions associated with these
 * events. There are two kinds of events: timers, and everything else.
 *
 * Timers are attached to the state in an array called timerActions. This array contains associative arrays which in
 * turn contain a Timer object 'timer', and a function 'actions' which returns an array of associative arrays which have
 * indices 'action', a string describing the action, and 'param', a single parameter, whatever that action requires.
 *
 * All other events are attached to a state as a property with a name equal to the name of the event. This property
 * is a function returning an array of associative arrays with at least index 'action', a string describing the action,
 * and usually 'param', whatever the action requires. The order of the actions in the array matters; they are taken
 * front to back in the message center. Especially make sure always to put a load state action at the very end only.
 */


function State()
{
	this.timerActions = new Array();
	this.name = "Unnamed state";

	/**
	 * Starts the first timer in the timer list.
	 * 
	 * @returns Nothing
	 */
	this.startTimer = function()
	{
		if(this.timerActions.length > 0) this.timerActions[0]['timer'].start();
	};


	this.hasRunningTimers = function()
	{
		return this.timerActions.length > 0;
	};
	
	
	/* Debugging */
	this.timerActionsToString = function()
	{
		var result = "";
		for(var i = 0; i < this.timerActions.length; i++)
		{
			if(i !== 0) result += ", ";
			result += "[" + i + "]:" + this.timerActions[i]['timer'].duration;
		}
		return result;
	};
}


/* Start state, before the game has even finished loading */
function StartState()
{
	State.call(this);
	this.name = "Start";

	/* Event actions */
	this.gameLoaded = function()
	{
		return new Array({action: 'load state', param: new TryMoveBoxesState()});
	};
}


/* State before the user has done anything, at all, ever */
function TryMoveBoxesState()
{
	State.call(this);
	this.name = "Try move boxes";

	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'tryMoveBoxesState0'});}});
	this.timerActions.push({timer: new Timer(6), actions: function(){ return new Array(
			{action: 'show message', param: 'tryMoveBoxesState1'});}});
	this.timerActions.push({timer: new Timer(10), actions: function(){ return new Array(
			{action: 'show message', param: 'tryMoveBoxesState2'});}});
	this.timerActions.push({timer: new Timer(10), actions: function(){ return new Array(
			{action: 'show message', param: 'tryMoveBoxesState3'});}});

	/* Event actions */
	this.boxSelected = function()
	{
		return new Array(
				{action: 'set done', param: 'singleBoxSelection'},
				{action: 'load state', param: new FirstBoxSelectedState()});
	};
}


/* State when the user has just managed to select their first box ever */
function FirstBoxSelectedState()
{
	State.call(this);
	this.name = "First box selected";

	this.message = 'firstBoxSelectedState0';

	/* Event actions */
	this.boxAddedToContainer = function()
	{
		return new Array(
			{action: 'set done', param: 'droppingInContainer'},
			{action: 'load state', param: new FirstBoxInContainerState()});
	};
}


/* State where the user has just managed to put their first box in a container ever */
function FirstBoxInContainerState()
{
	State.call(this);
	this.name = "First box in container";
	
	this.message = 'firstBoxInContainerState0';
	this.messageOptions = {'number1' : game.currentLevel.divisor - 1, 'number2' : game.currentLevel.dividend - 1};

	/* Event actions */
	this.roundVictory = function()
	{
		game.levelGenerator.setMinContainers(1);
		return new Array({action: 'load state', param: new FirstRoundCompleteState(game.currentLevel.divisor)});
	};
}


/* State after loading the second round ever, just after succesfully completing (that's actually the only way of
* completeing) the first ever round. The user is now in Level 1, Round 2 */
function FirstRoundCompleteState(lastRoundNumber)
{
	State.call(this);
	this.name = "First round complete";

	this.message = 'firstRoundCompleteState0';
	this.messageOptions = {'number1' : lastRoundNumber};
	this.timerActions.push({timer: new Timer(6), actions: function(){ return new Array(
			{action: 'show message', param: 'firstRoundCompleteState1',
			options: {'number1' : game.currentLevel.dividend, 'number2' : game.currentLevel.divisor}});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message',
			param: 'firstRoundCompleteState2'});}});

	this.levelVictory = function()
	{
		game.levelGenerator.setMaxContainers(6);
		return new Array({action: 'load state', param: new EnterDifficulty2State()});
	};
}


/* State just after entering the first round of difficulty 2. */
function EnterDifficulty2State()
{
	State.call(this);
	this.name = "Enter difficulty 2";

	this.message = 'enterDifficulty2State0';
	this.timerActions.push({timer: new Timer(12), actions: function(){ return new Array(
			{action: 'show message', param: 'enterDifficulty2State1'});}});
	this.timerActions.push({timer: new Timer(12), actions: function(){ return new Array(
			{action: 'show message', param: 'enterDifficulty2State2'});}});

	/* Event actions */
	this.roundVictory = function()
	{
		return new Array({action: 'load state',
				param: new DidSimpleDivisionState(game.currentLevel.dividend, game.currentLevel.divisor)});
	};
}


/* State after entering the second round of difficulty 2 */
function DidSimpleDivisionState(lastRoundDividend, lastRoundDivisor)
{
	State.call(this);
	this.name = "Did simple division";

	this.message = 'didSimpleDivisionState0';
	this.messageOptions = {'lastRoundDividend' : lastRoundDividend, 'lastRoundDivisor' : lastRoundDivisor,
			'result' : Math.round(lastRoundDividend / lastRoundDivisor)};
	this.timerActions.push({timer: new Timer(12), actions: function(){ return new Array(
			{action: 'show message', param: 'didSimpleDivisionState1',
			options: {'lastRoundDividend' : lastRoundDividend, 'lastRoundDivisor' : lastRoundDivisor,
					'result' : Math.round(lastRoundDividend / lastRoundDivisor), 'writeOut' : false}});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'didSimpleDivisionState2',
			options: {'dividend' : game.currentLevel.dividend, 'divisor' : game.currentLevel.divisor, 'writeOut' : false}});}});

	/* Event actions */
	this.roundVictory = function()
	{
		return new Array({action: 'load state',
				param: new Difficulty2RepeatState(game.currentLevel.dividend, game.currentLevel.divisor)});
	};
}


/* State in ever round of difficulty 2 except for the first round. */
function Difficulty2RepeatState(lastRoundDividend, lastRoundDivisor)
{
	State.call(this);
	this.name = "Difficulty 2 repeat";
	
	this.message = 'difficulty2RepeatState0';
	this.messageOptions = {'lastRoundDividend' : lastRoundDividend, 'lastRoundDivisor' : lastRoundDivisor,
			'result' : Math.round(lastRoundDividend / lastRoundDivisor),
			'thisRoundDividend' : game.currentLevel.dividend, 'thisRoundDivisor' : game.currentLevel.divisor,
			'writeOut' : false};

	/* Event actions */
	this.roundVictory = function()
	{
		return new Array({action: 'load state',
				param: new Difficulty2RepeatState(game.currentLevel.dividend, game.currentLevel.divisor)});
	};
	this.levelVictory = function()
	{
		return new Array({action: 'load state', param: new EnterDifficulty3State()});
	};
}


/* This state is a hack. Because in this case different timers should be set depending on the number of boxes and
 * containers of this round, and the state is actually instantiated during the last round, just load a new state as soon
 * as this one is activated. */
function EnterDifficulty3State()
{
	State.call(this);
	this.name = "Enter difficulty 3";

	this.timerActions.push({timer: new Timer(0), actions: function(){ return new Array(
			{action: 'load state', param: new EnteredDifficulty3State});}});
}


/* State right after entering difficulty 3. Dependent on the level loaded, decide when to explain the box splitting. */
function EnteredDifficulty3State()
{
	State.call(this);
	this.name = "Entered difficulty 3";
	
	this.message = 'enteredDifficulty3State0';
	if(game.currentLevel.divisor > game.currentLevel.dividend)
	{
		/* There are more containers than boxes. This will be obvious quite quickly, so just start the explanation */
		this.timerActions.push({timer: new Timer(10), actions: function(){ return new Array(
				{action: 'show message', param: 'enteredDifficulty3State1'});}});
		this.timerActions.push({timer: new Timer(10), actions: function(){ return new Array(
				{action: 'load state', param: new ExplainBoxSplittingState()});}});
	}
	else
	{
		/* There are more boxes than containers. It won't be immediately obvious to little kids that this is not going
		 * to work. Start explaining if the user either never does anything for 25 seconds, or did some things but then
		 * stopped trying for 10 seconds. */
		this.timerActions.push({timer: new Timer(25), actions: function(){ return new Array(
				{action: 'load state', param: new PrepareExplainBoxSplittingState()});}});
		this.timerActions.push({timer: new Timer(15), actions: function(){ return new Array(
				{action: 'load state', param: new PrepareExplainBoxSplittingState()});}});

		/* Event actions */
		this.handleEvent = function(event)
		{
			if(event === 'boxSelected' || event === 'boxDragging')
			{
				/* If the users did something, either delete the first timer and start the second, or if that already
				 * happened restart the second (which is now the first). */
				if(this.timerActions.length === 2)
				{
					this.timerActions.splice(0, 1);
					this.startTimer();
				}
				else
				{
					// This restarts the timer
					this.startTimer();
				}
			}
		};
	}
	
	/* Event actions */
	this.depth0BoxSplit = function()
	{
		/* This shouldn't happen, but is for very fast players to avoid deadlock */
		return new Array({action: 'load state', param: new DidBoxSplittingState()});
	};
}


/* Guiding the message flow to the messages of ExplainBoxSplittingState */
function PrepareExplainBoxSplittingState()
{
	State.call(this);
	this.name = "Prepare explain box splitting";
	
	this.message = 'prepareExplainBoxSplittingState0';
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'load state', param: new ExplainBoxSplittingState()});}});
	
	/* Event actions */
	this.depth0BoxSplit = function()
	{
		/* This shouldn't happen, but is for very fast players to avoid deadlock */
		return new Array({action: 'load state', param: new DidBoxSplittingState()});
	};
}


/* Explain how to split boxes */
function ExplainBoxSplittingState()
{
	State.call(this);
	this.name = "Explain box splitting";
	
	this.message = 'explainBoxSplittingState0';
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'explainBoxSplittingState1'});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'explainBoxSplittingState2'});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'explainBoxSplittingState3'});}});
	
	this.depth0BoxSplit = function()
	{
		return new Array({action: 'load state', param: new DidBoxSplittingState()});
	};
}


/* User split their first box. */
function DidBoxSplittingState()
{
	State.call(this);
	this.name = "Did box splitting";
	
	this.message = 'didBoxSplittingState0';
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'didBoxSplittingState1'});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'didBoxSplittingState2'});}});
			
	/* Event actions */
	this.roundVictory = function()
	{
		return new Array({action: 'load state',
				param: new DidOneDecimalDivisionState(game.currentLevel.dividend, game.currentLevel.divisor)});
	};
}


/* User completed the first round with a one decimal division. */
function DidOneDecimalDivisionState(lastRoundDividend, lastRoundDivisor)
{
	State.call(this);
	this.name = "Did one decimal division";
	
	this.message = 'didOneDecimalDivisionState0';
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'didOneDecimalDivisionState1',
			options: {'lastRoundDividend' : lastRoundDividend, 'lastRoundDivisor' : lastRoundDivisor,
			'result' : Math.round(lastRoundDividend * 10 / lastRoundDivisor) / 10, 'writeOut' : false}});}});
	this.timerActions.push({timer: new Timer(8), actions: function(){ return new Array(
			{action: 'show message', param: 'didOneDecimalDivisionState2',
			options: {'dividend' : game.currentLevel.dividend, 'divisor' : game.currentLevel.divisor, 'writeOut' : false}});}});

	/* Event actions */
	this.levelVictory = function()
	{
		return new Array({action: 'load state', param: new EnterDifficulty4State()});
	};
}


/* Enter difficulty 4 */
function EnterDifficulty4State()
{
	State.call(this);
	this.name = "Enter difficulty 4";
	
	messageCenter.explanationCenter.setApplicable('depth1BoxSplit');
	
	/* Event actions */
	this.levelVictory = function()
	{
		return new Array({action: 'load state', param: new EnterDifficulty5State()});
	};
}


/* Enter difficulty 5 */
function EnterDifficulty5State()
{
	State.call(this);
	this.name = "Enter difficulty 5";
}


/* Set up inheritance */
StartState.prototype = Object.create(State.prototype);
TryMoveBoxesState.prototype = Object.create(State.prototype);
FirstBoxSelectedState.prototype = Object.create(State.prototype);
FirstBoxInContainerState.prototype = Object.create(State.prototype);
FirstRoundCompleteState.prototype = Object.create(State.prototype);
EnterDifficulty2State.prototype = Object.create(State.prototype);
DidSimpleDivisionState.prototype = Object.create(State.prototype);
Difficulty2RepeatState.prototype = Object.create(State.prototype);
EnterDifficulty3State.prototype = Object.create(State.prototype);
EnteredDifficulty3State.prototype = Object.create(State.prototype);
PrepareExplainBoxSplittingState.prototype = Object.create(State.prototype);
ExplainBoxSplittingState.prototype = Object.create(State.prototype);
DidBoxSplittingState.prototype = Object.create(State.prototype);
DidOneDecimalDivisionState.prototype = Object.create(State.prototype);
EnterDifficulty4State.prototype = Object.create(State.prototype);
EnterDifficulty5State.prototype = Object.create(State.prototype);
