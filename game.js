// configuration
RESOURCE_PATH = 'lang/';
if(localStorage.language === undefined) localStorage.language = 'en';
if(localStorage.musicEnabled === undefined) localStorage.musicEnabled = 'true';
if(localStorage.speechEnabled === undefined) localStorage.speechEnabled = 'false';

/* Whenever you're ready */
$(document).ready(createResource(localStorage.language));

/* Canvas information */
var canvas;			// DOM canvas element
var resized = true;	// True if the canvas needs to be resized on next draw
var context;		// Drawing context
var game;			// Game object
var music;			// Background music
var input;			// Input handling
var messageCenter;	// MessageCenter object
var canvasPosition;	// set when needed by findLocalCursor, unset on resize
var language;
var R;				// Contains all the strings needed for the set language
var endGame;		// End game animation

function Game()
{
	this.positionOnDown = new Point(0, 0);	// Location of the cursor at last MouseDown action
	this.freeBoxes;							// All boxes outside a container
	this.containers;						// All containers
	this.rubberBand = undefined;			// Contains a RubberBand whenever rubber band selection in progress
	this.multiSelectGroup = undefined;		// Contains all boxes selected using multiselect
	this.movingSelection = false;			// True when the selected boxes are being moved

	/* Level settings */
	this.levelGenerator;	// The level factory
	this.currentLevel;		// Described the current difficulty etc
	this.currentRound;		// The how manieth round of this difficulty the player is in
	this.mistakeMade;		// Whether a mistake was made in the current round
	this.splash;
	this.osd = new OSD(this);

	/* Graphics settings */
	// Space in between containers as ratio of container width
	this.CONTAINER_SEPARATION = 0.5;
	// As ratio of height of the canvas
	this.CONTAINER_MAX_WIDTH = 0.2;
	// Minimum amount of rounds to play on any difficulty
	this.MINIMUM_ROUNDS_PER_DIFFICULTY = 3;


	/**
	 * Checks whether the current state of the game is a victory state. Mistakes are marked in here.
	 *
	 * @returns {Boolean}	True in case the game is in victory state, false otherwise
	 */
	this.checkVictoryCondition = function()
	{
		this.checkForMistake();
		var contentsFirstContainer = new Array();

		/* Victory means every container has the same number of boxes and there are no free boxes */
		// FIXME of course people are not supposed to put ten small boxes into the same container
		if(this.freeBoxes.length !== 0) return false;

		/* Check the contents of the first container */
		for(var i = 0; i < this.containers[0].boxes.length; i++)
		{
			var depth = this.containers[0].boxes[i].depth;
			contentsFirstContainer[depth] = contentsFirstContainer[depth] + 1 || 1;
		}

		/* Verify that all other containers contain exactly the same */
		// FIXME of course people are not supposed to put ten small boxes into the same container
		for(var i = 1; i < this.containers.length; i++)
		{
			var boxes = this.containers[i].boxes;
			var contentsCurrentContainer = new Array();

			for(var j = 0; j < boxes.length; j++)
			{
				var depth = this.containers[i].boxes[j].depth;
				contentsCurrentContainer[depth] = contentsCurrentContainer[depth] + 1 || 1;
			}

			if(!equalArrays(contentsCurrentContainer, contentsFirstContainer)) return false;
		}

		return true;
	};


	/**
	 * Sets mistakeMade to true if the current state cannot become a victory state without taking a box out of a
	 * container.
	 *
	 * @returns Nothing
	 */
	this.checkForMistake = function()
	{
		/* Already set? */
		if(this.mistakeMade) return;

		/* No container should have a value higher than this */
		var answer = this.currentLevel.dividend / this.currentLevel.divisor;

		for(var i = 0; i < this.containers.length; i++)
		{
			if(this.containers[i].getContainedValue() > answer) this.mistakeMade = true;
		}
	};


	this.destroyMultiSelectGroup = function()
	{
		if(!this.multiSelectGroup) return;

		for(var i = 0; i < this.multiSelectGroup.boxes.length; i++)
		{
			this.multiSelectGroup.boxes[i].resetColor();
			this.freeBoxes.push(this.multiSelectGroup.boxes[i]);
		}
		this.multiSelectGroup = undefined;
		this.movingSelection = false;
	};


	this.resize = function()
	{
		this.sizeContainers();
		this.sizeBoxes();
	};


	/**
	 * Places numBoxes in the free space above the containers. If random is true, they are randomly distributed there and
	 * may be overlapping. If random is false, they are placed in a line.
	 *
	 * @param	{Number}	numBoxes	The number of boxes to put in the freeBoxes array
	 * @param	{Number}	random		Whether they should be placed randomly, or in a line
	 *
	 * @returns nothing
	 */
	this.initFreeBoxes = function(numBoxes, random)
	{
		//Reset the freeBoxes array
		this.freeBoxes = new Array();
		/* All boxes should fit in a line. But they should be at least 50 pixels wide. And at most half the size of a
		 * container */
		var boxWidth = getBoxSize(0);
		var boxesPerLine = canvas.clientWidth / boxWidth;

		for(var i = 0; i < numBoxes; i++)
		{
			var box = new Box();
			box.width = box.height = boxWidth;
			
			// center boxes
			var offsetX = 0;
			var offsetY = 0;
			if(canvas.clientWidth > boxWidth * numBoxes)
			{
				offsetX = (canvas.clientWidth - boxWidth * numBoxes) / 2;
			}
			
			// 100px margin if canvas is higher than 600px
			if(canvas.clientHeight > 600)
			{
				offsetY = 100;
			}
			
			var x = offsetX + (i % boxesPerLine) * boxWidth;
			var y = offsetY + Math.floor(i / boxesPerLine) * boxWidth;
			box.setPosition(new Point(x, y));
			
			this.freeBoxes.push(box);
		}
	};


	this.sizeBoxes = function()
	{
		/* Size the free boxes */
		for(var i = 0; i < this.freeBoxes.length; i++)
		{
			this.freeBoxes[i].recalculatePosition();
		}
		/* Size the selected boxes */
		if(this.multiSelectGroup)
		{
			for(var i = 0; i < this.multiSelectGroup.boxes.length; i++)
			{
				this.multiSelectGroup.boxes[i].recalculatePosition();
			}
		}
		/* Size the boxes inside the containers */
		for(var i = 0; i < this.containers.length; i++)
		{
			this.containers[i].sizeBoxes();
		}
	};


	/**
	 * Initializes the containers.
	 *
	 *
	 * @returns {undefined}
	 */
	this.initContainers = function()
	{
		this.containers = new Array();
		for(var i = 0; i < this.currentLevel.divisor; i++) this.containers.push(new Container());
		this.sizeContainers();
	};


	/**
	 * Takes the array of containers of the current game and changes their dimensions to what they're supposed to be.
	 * They all get a position at half their height from the bottom of the canvas, and a size dependent on the canvas
	 * size and the amount of containers. Containers are square. The maximum width and separation can be set in this
	 * function.
	 *
	 * @returns nothing
	 */
	this.sizeContainers = function()
	{
		var cx = canvas.width;
		var cy = canvas.height;
		var numContainers = this.currentLevel.divisor;

		/* There is no minimum size, they must simply fit */
		var edgeLength = Math.min(cx / (numContainers * (1 + this.CONTAINER_SEPARATION)), cy * this.CONTAINER_MAX_WIDTH);
		for(var i = 0; i < numContainers; i++)
		{
			var container = this.containers[i];
			container.edgeLength = edgeLength;
			/* containersBound is the width from the start of the leftmost to the end of the rightmost container */
			var containersBound = (numContainers * edgeLength + (numContainers - 1) * this.CONTAINER_SEPARATION * edgeLength);
			/* Position the containers relative to the middle of the canvas */
			container.position.x = cx / 2 - containersBound / 2 + i * (edgeLength + this.CONTAINER_SEPARATION * edgeLength);
			container.position.y = cy - (1 + this.CONTAINER_SEPARATION / 2) * edgeLength;
		}
	};


	/**
	 * Does all preparations for the new level to be loaded at the next draw. Draw is not called.
	 *
	 * @param	{Boolean}	splashAction	To splash or not to splash
	 * @param	{Boolean}	jumping			Optional, got here through settings menu?
	 *
	 * @returns nothing
	 */
	this.loadNewLevel = function(splashAction, jumping)
	{
		if(jumping) messageCenter.handleEvent('difficultyJump');
		/* Check for difficulty advancement */
		else if(!this.mistakeMade && this.currentRound >= this.MINIMUM_ROUNDS_PER_DIFFICULTY)
		{
			this.levelGenerator.difficulty++;
			this.currentRound = 1;
			messageCenter.handleEvent('levelVictory');
		}
		else if(messageCenter)
		{
			this.currentRound++;
			messageCenter.handleEvent('roundVictory');
		}

		/* Clean up */
		this.mistakeMade = false;

		/* Load new round */
		this.currentLevel = this.levelGenerator.generateLevel(this.currentLevel);
		this.initContainers(this.currentLevel.divisor);
		this.initFreeBoxes(this.currentLevel.dividend, false);

		/* Update the information for the user */
		$('#levelInfo').html(retrieveString('roundBoard', {'level' : this.currentLevel.difficulty, 'round' : this.currentRound, 'writeOut' : false}));
		document.getElementById('assignmentDescription').innerHTML = this.currentLevel.mission;
		resizeMessageCenter();

		if(splashAction)
		{
			this.splash = new Splash(
					canvas.width,
					canvas.height,
					this.currentLevel.difficulty,
					this.currentRound,
					this.currentLevel.mission
			);
		}
	};


	/**
	 * Splits the box with index index in the freeBoxes array into ten little boxes, deleting the original and adding these
	 * ten new ones to the array.
	 *
	 * @param	{int}	index		The id of the box that is to be split in ten
	 * @param	{bool}	random		Whether the new squares should be placed randomly in the area of the old one, or
	 *								ordered
	 * @param	{bool}	override	Optional. Splits box, even when it would be too deep.
	 *
	 * @returns nothing
	 */
	this.splitBox = function(index, random, override)
	{
		if(this.freeBoxes[index].depth >= this.currentLevel.difficulty - 2 && !override) return;
		messageCenter.handleEvent('depth' + this.freeBoxes[index].depth + 'BoxSplit');
		
		var bigBox = this.freeBoxes.splice(index, 1)[0];
		var littleBoxWidth = Math.min(bigBox.width, Math.max(bigBox.width / 2, 10));

		if(random)
		{
			/* Prepare the field the little boxes will be placed in. This has been split up in to very many
			 * subcaltulations in the hope of clarity. Place the little boxes within an area of width three times the
			 * little box width centered around the center of the big box, but make sure they're within the canvas,
			 * shrinking the area if necessary */
			var bigBoxCenterX = bigBox.getPosition().x + bigBox.width / 2;
			var bigBoxCenterY = bigBox.getPosition().y + bigBox.height / 2;
			var preferredAreaX = bigBoxCenterX - littleBoxWidth * 1.5;
			var preferredAreaY = bigBoxCenterY - littleBoxWidth * 1.5;

			/* The values that will be used: */
			var minX = Math.max(0, preferredAreaX);
			var maxX = Math.min(canvas.width - littleBoxWidth, preferredAreaX + 2 * littleBoxWidth); //no, 2 is correct
			var minY = Math.max(0, preferredAreaY);
			var maxY = Math.min(canvas.height - littleBoxWidth, preferredAreaY + 2 * littleBoxWidth);
		}

		for(var i = 0; i < 10; i++)
		{
			var littleBox = new Box();
			/* The little boxes should be one third the width of the big box. However, they should be at least 25 pixels
			 * wide but not if that's bigger than the original box.
			 */
			littleBox.width = littleBox.height = littleBoxWidth;
			littleBox.setDepth(bigBox.depth + 1);
			if(random)
			{
				littleBox.setPosition(new Point(minX + Math.random() * (maxX - minX), minY + Math.random() * (maxY - minY)));
			}
			else
			{
				// TODO implement
			}
			
			var addedToContainer = false;
			for(var k = 0; k < game.containers.length; k++)
			{
				if(game.containers[k].captures(littleBox))
				{
					game.containers[k].addBox(littleBox);
					addedToContainer = true;
					messageCenter.handleEvent('boxAddedToContainer');
					/* The boxes were added to the container. This is always the last action before a player can win.
					 * It is highly unlikely, and in the current implementation actually impossible, that this results
					 * in a win, but check anyway. */
					if(game.checkVictoryCondition())
					{
						game.loadNewLevel(true);
					}
				}
			}
			if(!addedToContainer) this.freeBoxes.push(littleBox);
		}
	};
	
	
	/**
	 * Loads a new level of the given difficulty. This will currently mess with the message center and explanation
	 * center severely and is only provided as a debugging tool.
	 * 
	 * @param {type} difficulty		Round 1 of this difficulty will be loaded
	 * @param {type} splashAction	Show a splash screen?
	 * @returns nothing
	 */
	this.loadDifficulty = function(difficulty, splashAction)
	{
		this.currentRound = 1;
		this.levelGenerator.difficulty = difficulty;
		this.loadNewLevel(splashAction, true);
	};
}

function draw()
{
	if(resized)
	{
		/* First resize the canvas element, then tell the game to do whatever it needs to do and unset the flag */
		resizeCanvas();
		game.resize();
		resizeMessageCenter();
		resized = false;
	}
	
	clearCanvas();

	/* Check for messages */
	messageCenter.checkTimers();
	
	/* Draw the game objects */
	for(var i = 0; i < game.containers.length; i++)	game.containers[i].draw();
	for(var i = 0; i < game.freeBoxes.length; i++) game.freeBoxes[i].draw();
	if(game.multiSelectGroup) game.multiSelectGroup.draw();
	drawContainerCounters();
	if(game.rubberBand) game.rubberBand.draw();
	if(game.osd) game.osd.draw();

	// Draw endgame, if necessary
	endGame.draw();
	
	/* Draw the splash, and remove it if it has expired */
	if(game.splash && !game.splash.draw()) game.splash = undefined;

	// request next frame
	if(window.requestAnimationFrame) window.requestAnimationFrame(draw);
	else if(window.mozRequestAnimationFrame) window.mozRequestAnimationFrame(draw);
	else if(window.webkitRequestAnimationFrame) window.webkitRequestAnimationFrame(draw);
}


/**
 * Reset every pixel on the canvas to the default background color, so the next frame can be drawn.
 * 
 * @returns nothing
 */
function clearCanvas()
{
	// clear screen in every way thinable to avoid memory leaks
	canvas.width = canvas.width;
	canvas.height = canvas.height;
	//context.clearRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = "rgba(64, 64, 64, 1)";
	context.fillRect(0, 0, canvas.width, canvas.height);
}


/**
 * Goes through all containers, drawing a number below them showing the value of the boxes inside.
 *
 * @returns {undefined}
 */
function drawContainerCounters()
{
	var textHeight = game.containers[0].edgeLength * game.CONTAINER_SEPARATION / 2;
	context.font = textHeight + "pt sans-serif";
	context.fillStyle = "orange";

	for(var i = 0; i < game.containers.length; i++)
	{
		var container = game.containers[i];
		/* Get the value and set the precision to avoid long numbers due to precision errors */
		var printValue = container.getContainedValue().toFixed(container.getMaxDepth());

		/* Find out how wide the text will be */
		var textWidth = context.measureText(printValue).width;

		/* Print */
		context.fillText(printValue, container.position.x + container.edgeLength / 2 - textWidth / 2,
				container.position.y + container.edgeLength + textHeight);
	}
}

function init()
{
	initSettings();
	
	/* Toggle twice to get everything set, but stick to the on/off setting that was in effect when starting the game */
	toggleSpeech();
	toggleSpeech();
	speak('Welcome to Divide!');

	/* Create the canvas */
	$('#canvas-container').append('<canvas id="game">Your browser sucks.</canvas>');
	canvas = $('canvas').get(0);
	context = canvas.getContext("2d");

	/* Create the game */
	game = new Game();

	/* Set the size before initializing the game */
	resizeCanvas();

	input = new Input(game);

	initMusic();
	
	document.querySelector('#settings-speech').onclick = function() {
		toggleSpeech();
	}

	endGame = new EndGame();

	/* Level settings, game initialization */
	// Start with a level with the same number of boxes as containers, and more than one container.
	game.levelGenerator = new LevelGenerator(1, 2, 7);
	game.levelGenerator.generateOptions();
	game.currentRound = 1;
	game.loadNewLevel(true);


	messageCenter = new MessageCenter();
	messageCenter.init();
	messageCenter.handleEvent('gameLoaded');

	/* Event handlers */
	window.addEventListener("mousemove", input.handleMouseMove);
	canvas.addEventListener("mousedown", input.handleMouseDown);
	window.addEventListener("mouseup", input.handleMouseUp);
	canvas.addEventListener("dblclick", input.handleDoubleClick);
	$(window).resize(function() {resized = true;});
	music.addEventListener('pause', function() {
		document.querySelector('#settings-music').onclick = function() {
			localStorage['musicEnabled'] = true;
			music.play();
		};
	});
	music.addEventListener('play', function() {
		document.querySelector('#settings-music').onclick = function() {
			localStorage['musicEnabled'] = false;
			music.pause();
		};
	});

	/* Chrome fixes */
	canvas.onselectstart = function(){ return false; };
	
	/* Start draw loop */
	if(window.requestAnimationFrame) window.requestAnimationFrame(draw);
	else if(window.mozRequestAnimationFrame) window.mozRequestAnimationFrame(draw);
	else window.alert("Your browser sucks");
}


/**
 * //TODO describe in detail what this function does
 * 
 * @returns nothing
 */
function initMusic()
{
	/* Music */
	music = new Audio();
	var track = Math.floor(Math.random() * 7);
	music.src = "music/track" + track + ".ogg"; // Track 0 from http://http://www.nosoapradio.us/ others from Neverball
	music.volume = 0.5;
	music.addEventListener('ended', function()
	{
		var track = Math.floor(Math.random() * 7);
		music.src = "music/track" + track + ".ogg";
		music.play();
	});

	if(localStorage['musicEnabled'] === 'true')
	{
		music.play();
		document.querySelector('#settings-music').checked = true;
	}
	else
	{
		document.querySelector('#settings-music').checked = false;
		document.querySelector('#settings-music').onclick = function() {
			localStorage['musicEnabled'] = true;
			music.play();
		};
	}
}


function initSettings()
{
	function level(num) 
	{
		game.loadDifficulty(num, true);
		$('#settings-overlay').fadeOut();
	}
	
	$('#level1').click(function(){level(1);});
	$('#level2').click(function(){level(2);});
	$('#level3').click(function(){level(3);});
	$('#level4').click(function(){level(4);});
	$('#level5').click(function(){level(5);});
	
	// Handle settings

	$('[href=#settings]').click(function() { $('#settings-overlay').fadeIn(); return false; });
	$('#settings-close').click(function() { $('#settings-overlay').fadeOut(); });
	$('#settings-language').change(function() { localStorage.language = $('option:selected', this).val(); });
	$('[href=#refresh]').click(function() { window.location.reload(); return false; });
}
