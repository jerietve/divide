window.addEventListener("DOMContentLoaded", invalidateCanvasPosition, false);
window.addEventListener("resize", invalidateCanvasPosition, false);

/* Add method to String: */
/**
 * Returns a new String equal to the old string but with s inserted at index, replacing the first replace characters
 * after that point.
 *
 * @param {Number} index	The index at which to insert s
 * @param {String} s		The string to insert
 * @param {Number} replace	Optional; the number of characters to delete after index
 * @returns {String}		The new string
 */
String.prototype.insert = function(index, s, replace)
{
	replace = replace || 0;
	return this.slice(0, index) + s + this.slice(index + replace);
};


/* add centerFillText function to all canvas contexts */
CanvasRenderingContext2D.prototype.centerFillText = function(txt, y, maxWidth) 
{
	var txtWidth = this.measureText(txt).width;

	var x;
	if(txtWidth < maxWidth)
	{
		x = (maxWidth - txtWidth) / 2;
	}
	else
	{
		x = 0; 
	}

	this.fillText(txt, x, y, maxWidth);
};


function randInt(start, end)
{
	return Math.floor(Math.random()*(end - start + 1))+start;
}

Math.seed = 6;
 
// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
Math.seededRandInt = function(max, min) {
    max = max || 255;
    min = min || 0;
 
    Math.seed = (Math.seed * 9301 + 49297) % 233280;
    var rnd = Math.seed / 233280;
 
    return Math.ceil(min + rnd * (max - min));
};

function findPosition(element)
{
	var cursorLeft = 0;
	var cursorTop = 0;

	if(element.offsetParent)
	{
		do
		{
			cursorLeft += element.offsetLeft;
			cursorTop += element.offsetTop;
		}
		while(element = element.offsetParent);

		return new Point(cursorLeft, cursorTop);
	}

	return undefined;
}

function findLocalCursor(e)
{
	if(canvasPosition === undefined)
	{
		var canvas = document.getElementsByTagName("canvas")[0];
		canvasPosition = findPosition(canvas);
	}

	//var mouseX = e.pageX - canvasPosition.x;
	//var mouseY = e.pageY - canvasPosition.y;
	return new Point(e.pageX, e.pageY).sub(canvasPosition);
}

function findGlobalCursor(e)
{
	return new Point(e.pageX, e.pageY);
}


/**
 * Check if the point is within the canvas.
 * 
 * @param {Point} point
 * @returns {Boolean}	true iff the point is within the canvas
 */
function withinCanvas(point)
{
	return point.x >= 0 && point.x < canvas.width && point.y >= 0 && point.y < canvas.height;
}

function invalidateCanvasPosition()
{
	canvasPosition = undefined;
}


/**
 * Returns true if both arrays contain exactly the same values in the same locations, false otherwise
 *
 * @param {Array} array1
 * @param {Array} array2
 *
 * @returns {Boolean}
 */
function equalArrays(array1, array2)
{
	if(array1.length !== array2.length) return false;
	for(i = 0; i < array1.length; i++)
	{
		if(array1[i] !== array2[i]) return false;
	}

	return true;
}


function getBoxSize(depth)
{
	var biggestBoxSize = Math.min(Math.max(canvas.clientWidth /	game.currentLevel.dividend, 50),
			game.containers[0].edgeLength / 2);
	return Math.min(biggestBoxSize,	Math.max(biggestBoxSize / Math.pow(2, depth), 10));
}


/**
 * Resizes the canvas to fill the width and height of the containing column.
 *
 * @returns nothing
 */
function resizeCanvas()
{
	var canvas = $('canvas');
	var aside = $('aside');
	var body = $('body');
	var newWidth = body.innerWidth() - aside.innerWidth();
	var newHeight = window.innerHeight;
	canvas.width(newWidth);
	canvas.height(newHeight);
	game.canvas = canvas.get(0);
	game.canvas.width = newWidth;
	game.canvas.height = newHeight;
}


function resizeMessageCenter()
{
	var newHeight = $('body').innerHeight();

	$('aside').children().each(function() {
		if($(this).attr('id') !== 'messageCenter')
		{
			newHeight -= $(this).outerHeight();
		}
	});

	$('#messageCenter').css('height', newHeight);
}


/**
 * Turn the text-to-speech function on or off
 * 
 * @returns nothing
 */
function toggleSpeech() 
{
	if(localStorage['speechEnabled'] === 'true')
	{
		localStorage['speechEnabled'] = false;
		document.querySelector('#settings-speech').checked = false;
		// disabled speak function
		window.tmpSpeak = speak;
		speak = function() {};
	}
	else
	{
		localStorage['speechEnabled'] = true;
		document.querySelector('#settings-speech').checked = true;
		if(window.tmpSpeak !== undefined)
		{
			speak = window.tmpSpeak;
		}
	}
}


/**
 * Appends the message to the bottom of the message center and scrolls down.
 *
 * @param {String} messageName	The name of the xml string element containing the messsage to be printed
 * @param {Array<String>}	options	Optional; see completeString
 * @returns Nothing
 */
function printMessage(messageName, options)
{
	var message = retrieveString(messageName, options);
	speak(message);
	// place message and make transparent
	$('#messageCenter').append('<p>' + message + '</p>');
	var p = $("p:last-child", $('#messageCenter'));
	p.css('opacity', 0);
	p.css('background-color', '#668');

	// Scroll down to the bottom of the message center
	$('#messageCenter').stop().animate({scrollTop: $('#messageCenter')[0].scrollHeight}, 2000);

	// fade message in
	window.setTimeout(function() { p.animate({ opacity: 1 }, 400); }, 1000);

	// flash message
	window.setTimeout(function() { p.css('background-color', '#222'); }, 1400);

	this.lastMessageTime = new Date();
}


/**
 * Checks if a certain time has passed.
 *
 * @param {Number} seconds	The amount of seconds that should have passed
 * @param {Number} since	The time from which to check, (gotten from Date.now() for example)
 * @returns {Boolean}		True if enough seconds have passed, false otherwise
 */
function itsBeen(seconds, since)
{
	return Date.now() > since + seconds * 1000;
}


/**
 * 
 * @param {Point} from1	The upper left corner of the first rectangle
 * @param {Point} to1	The lower right corner of the first rectangle
 * @param {Point} from2	The upper left corner of the second rectangle
 * @param {Point} to2	The lower right corner of the second rectangle
 * @returns {Boolean} true iff the rectangles intersect
 */
function rectanglesIntersect(from1, to1, from2, to2)
{
	if(to1.x < from2.x || from1.x > to2.x || to1.y < from2.y || from1.y > to2.y) return false;
	else return true;
}


/* Auxiliary classes */

/**
 * A coordinate class. Sometimes it's a point sometimes it's a vector who knows :-).
 *
 * @param {type} x
 * @param {type} y
 */
function Point(x, y)
{
	this.x = x;
	this.y = y;


	/**
	 * Checks if this point is within the rectangle spanned by opposing corner Points point1 and point2. On the line is
	 * considered in.
	 *
	 * @param {Point} point1
	 * @param {Point} point2
	 * @returns {Boolean} True if so, false if not
	 */
	this.inRectangle = function(point1, point2)
	{
		/* This point is within a rectangle of point1 and point2 if in both x and y separately the value of point1 and
		 * point2 surround this point */
		if(!(point1.x >= this.x && point2.x <= this.x || point1.x <= this.x && point2.x >= this.x)) return false;
		else if(point1.y >= this.y && point2.y <= this.y || point1.y <= this.y && point2.y >= this.y) return true;
		else return false;
	};


	/**
	 * Returns a Point describing the subtraction of otherPoint from this
	 *
	 * @param {Point} otherPoint
	 * @returns {Point}
	 */
	this.sub = function(otherPoint)
	{
		if(otherPoint === undefined)
		{
			throw new Error("otherPoint undefined!");
		}
		return new Point(this.x - otherPoint.x, this.y - otherPoint.y);
	};


	/**
	 * Subtracts otherPoint from this Point
	 *
	 * @param {Point} otherPoint
	 * @returns {Point}
	 */
	this.subBang = function(otherPoint)
	{
		this.x -= otherPoint.x;
		this.y -= otherPoint.y;
	};


	/**
	 * Returns a Point obtained by multiplying this 'point' (vector) by a.
	 *
	 * @param {Number} a
	 * @returns {Point}
	 */
	this.mul = function(a)
	{
		return new Point(this.x * a, this.y * a);
	};


	/**
	 * Multiplies this 'point' (vector) by a.
	 *
	 * @param {Number} a
	 * @returns nothing
	 */
	this.mulBang = function(a)
	{
		this.x = this.x * a;
		this.y = this.y * a;
	};


	/**
	 * Returns the Point (vector) obtained by adding another Point (vector) to this Point
	 *
	 * @param {Point} otherPoint
	 * @returns {Point}
	 */
	this.add = function(otherPoint)
	{
		return new Point(this.x + otherPoint.x, this.y + otherPoint.y);
	};


	/**
	 * Checks if the coordinates have the same values
	 *
	 * @param {Point} otherPoint
	 */
	this.equals = function(otherPoint)
	{
		return this.x === otherPoint.x && this.y === otherPoint.y;
	};


	this.toString = function()
	{
		return "(" + this.x + ", " + this.y + ")";
	};
}


/**
 *
 * @param {Number} duration	The amount of seconds after the start the timer expires
 * @returns {Timer}
 */
function Timer(duration)
{
	this.started = false;
	this.startTime;
	this.duration = duration;
	this.endTime;


	this.start = function()
	{
		this.startTime = new Date();
		this.endTime = new Date();
		this.endTime.setSeconds(this.startTime.getSeconds() + this.duration);
		this.started = true;
	};


	this.hasExpired = function()
	{
		if(!this.started) return false;
		return Date.now() > this.endTime;
	};
}
